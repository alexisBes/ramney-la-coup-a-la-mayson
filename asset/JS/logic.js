function ChangeFormulaire(name) {
    if (name == "organisateur") {
        document.getElementById('identifiant').innerHTML = '<div class="col-lg-1"><label for="name">identifiant: </label></div><div class="col-lg-1"><input type="text" name="name" id="name" required></div>';
    } else {
        document.getElementById('identifiant').innerHTML = '<div class="col-lg-1"><label for="name">nom: </label></div><div class="col-lg-3"><input type="text" name="name" id="name" required></div><div class="col-lg-8" style="height:26px"></div><div class="col-lg-1"><label for="surname">Prenom: </label></div><div class="col-lg-1"><input type="text" name="surname" id="surname" required></div>';
    }
}

function allowDrop(ev){
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.textContent);
    ev.dataTransfer.setData("id",ev.target.id);
}

function drop(ev,name)
{
    ev.preventDefault(); 
    if (document.getElementById("useless")) {
        document.getElementById("useless").remove();
    }
    var data = ev.dataTransfer.getData("text");
    var id = ev.dataTransfer.getData("id");
    document.getElementById(id).remove();
    document.getElementById("dropZone_"+name).innerHTML+=  "<div onClick='eraseDropElement(event)' name='joueurEnregistrer'><input type=\"hidden\" name=\"joueurs["+name+"][]\" value="+id+"></input><label>"+data+"</label></div>";
}

function changeJoueur() {
    equipe =document.getElementById("equipe").value;
    usefulData = document.getElementById('dataUseful');
    arrayJoueur = JSON.parse(usefulData.dataset.joueur);
    arrayEquipe = JSON.parse(usefulData.dataset.equipe);
    index = 0;
    for (index = 0; index < arrayEquipe.length; index++) {
        const element = arrayEquipe[index];
        if (equipe == element.nom) {
            break;
        }
    }
    divJoueur =document.getElementById("joueurASelectionner");
    divArbitre = document.getElementById("joueurCapitaine");
    divJoueur.innerHTML = "";
    divArbitre.innerHTML = "";
    do {
        allSavePlayer = document.getElementsByName("joueurEnregistrer");
        for (i = 0; i < allSavePlayer.length; i++) {
            allSavePlayer[i].remove();
        }
    } while (allSavePlayer.length != 0);
    arrayJoueur[index].forEach(element => {
        divJoueur.innerHTML += '<div id="'+element.id+'" draggable="true" ondragstart="drag(event)"><label id="JoueurDansListe">'+element.nom+'('+element.numero+')</label></div>';
        divArbitre.innerHTML+= '<option value="'+element.nom+' '+element.prenom+'"></option>'
    });
}

function eraseDropElement(ev) {
    idJoueur = ev.target.parentNode.firstChild.value;
    usefulData = document.getElementById('dataUseful');
    arrayJoueur = JSON.parse(usefulData.dataset.joueur);
    var strToSend = ""
    if( document.getElementById("equipe")){
        arrayEquipe = JSON.parse(usefulData.dataset.equipe);
        var index = 0;
        console.log(equipe);
        for (index = 0; index < arrayEquipe.length; index++) {
            const element = arrayEquipe[index];
            if (equipe.value == element.nom) {
                break;
            }
        }
        indexPLayer =0;
        for (indexPLayer = 0; indexPLayer < arrayJoueur[index].length; indexPLayer++) {
            const element = arrayJoueur[index][indexPLayer];
            console.log(element);
            if (element.id == idJoueur) {
                strToSend = '<div id="'+element.id+'" draggable="true" ondragstart="drag(event)"><label id="JoueurDansListe">'+element.nom+'('+element.numero+')</label></div>';
                break;
            }
        }
    }else{
        indexPLayer = 0;
        for (indexPLayer = 0; indexPLayer < arrayJoueur.length; indexPLayer++) {
            const element = arrayJoueur[indexPLayer];
            if (element.id == idJoueur) {
                strToSend = '<div id="'+element.id+'" draggable="true" ondragstart="drag(event)"><label id="JoueurDansListe">'+element.nom+'('+element.numero+')</label></div>';
                break;
            }
        }
    }
    divJoueur =document.getElementById("joueurASelectionner");
    divJoueur.innerHTML+=strToSend;
    ev.target.parentNode.remove();
}

function checkFormulaireFDM() {
    var size = document.getElementsByName("joueurEnregistrer").length;
    var nbRemplacant = document.getElementById("dropZone_remplacant").childElementCount;
    var nbGardien = document.getElementById("dropZone_Gardien").childElementCount;
    size-=nbRemplacant;
    bValide = true;
    reason = "";
    if (size !=11) {
        reason+="Vous devez entrer 11 joueur, vous en avez ajouté "+size+"\n";
        bValide = false;
    }
    if (nbRemplacant<3) {
        reason+="Il faut au moins 3 remplacant displonible pour un match, vous en avez ajouté "+nbRemplacant+"\n";
        bValide = false;
    }
    if (nbGardien !=1) {
        reason+="Il vous faut un seul gardien pour le match, vous en avez rentré "+nbGardien+"\n";
        bValide = false;
    }
    if (bValide) {
        return true;
    } else {
        alert(reason);
        return false;
    }
}

function checkFormulaireCreateFDM() {
    arbitre0 = document.getElementById("arbitre0").value;
    arbitre1 = document.getElementById("arbitre1").value;
    arbitre2 = document.getElementById("arbitre2").value;
    equipe1 = document.getElementById("equipe1").value;
    equipe2 = document.getElementById("equipe2").value;
    isValid = true;
    reason = "";
    if (arbitre0 == arbitre1) {
        reason += "L'arbitre principal et le premier arbitre secondaire sont les mêmes \n";
        isValid = false;
    }
    if (arbitre0 == arbitre2) {
        reason += "L'arbitre principal et le second arbitre secondaire sont les mêmes \n";
        isValid = false;
    }
    if (arbitre1 == arbitre2) {
        reason += "Les deux arbitres secondaire sont les mêmes \n";
        isValid = false;
    }
    if (equipe1 == equipe2) {
        reason+="Les deux equipes sont identiques \n";
        isValid = false;
    }
    if (isValid) {
        return true;
    } else {
        alert(reason);
        return false;
    }
}

function updateScore(idEquipe)
{
    var strScore =document.getElementById("ContenuEncadreScore").innerHTML;
    partScore = strScore.split('-');
    score = parseInt(partScore[idEquipe],10);
    score += 1;
    partScore[idEquipe] = score;
    document.getElementById("ContenuEncadreScore").innerHTML = partScore[0]+"-"+partScore[1];
    console.log(partScore);
}

function SendFormInAjax() {
    var form =document.getElementById('formEvent');
    var joueur = form.joueur.value;
    var time = form.time.value;
    var typeEvent = form.typeEvent.value;
    elementString = joueur.split(' ');
    nom = elementString[0];
    prenom = elementString[1];
    numero = elementString[2];
    var value = document.getElementById('equipe').value;
    var data = document.getElementById('data');
    equipe1 = JSON.parse(data.dataset.equipe1);
    equipe2 = JSON.parse(data.dataset.equipe2);
    idMatch = data.dataset.idmatch;
    var joueur;
    idEquipe =-1;
    if (value==equipe1.id) {
        joueur = JSON.parse(data.dataset.joueurs1);
        idEquipe =0;
    } else {
        joueur = JSON.parse(data.dataset.joueurs2);
        idEquipe =1;
    }
    idJoueur = -1;
    joueur.forEach(element => {
        if (element.nom == nom &&element.prenom == prenom&&element.numero == numero) {
            idJoueur = element.id;
        }
    });
    allevent = JSON.parse(data.dataset.event);
    idEvent = -1;
    allevent.forEach(element =>{
        if (element.evenType == typeEvent) {
            idEvent = element.id;
        }
    })
    if (idEvent === -1) {
        alert("Joueur inconnu");
        return false;
    }
    if (parseInt(idEvent,10)===1) {
        updateScore(idEquipe);
    }
    if (idJoueur === -1) {
        alert("Joueur inconnu");
        return false;
    }
    var httpRequest = new XMLHttpRequest();
    var formData = new FormData();
    
    formData.append('controller','Async');
    formData.append('idJoueur',idJoueur);
    formData.append('time',time);
    formData.append('idEvent',idEvent);
    formData.append('idMatch',idMatch);
    selectResult = document.getElementById('EventArea');
    httpRequest.onreadystatechange = function(){
        if ( this.readyState == 4 && this.status == 200 ) {
            selectResult.innerHTML += this.responseText; // Display the result inside result element.
        }
    };

    httpRequest.open('POST','index.php');
    httpRequest.send(formData);
    document.getElementById('formulaireAera').innerHTML = "";
    return false;
}

function AddFomulaire(){
    var strFormulaire ='<select id="equipe" name="equipe" onchange="changeJoueurEvenement()">';
    var data = document.getElementById('data');
    equipe1 = JSON.parse(data.dataset.equipe1);
    equipe2 = JSON.parse(data.dataset.equipe2);
    strFormulaire += '<div class="row"><option value="'+equipe1.id+'">'+equipe1.nom+'</option><option value="'+equipe2.id+'">'+equipe2.nom+'</option></select><div class="row">';
    strFormulaire+= '<form id="formEvent" onsubmit="return SendFormInAjax()" action="" method="post">'
    strFormulaire+= '<div><div class="row"><div class="col-lg-2"><label>Evenement :</label></div><div class="col-lg-2"><input list="allEvent" name="typeEvent" id="typeEvent" required></input></div><datalist id="allEvent">';
    allEvent = JSON.parse(data.dataset.event);
    allEvent.forEach(element => {
        strFormulaire += '<option id='+element.id+' name='+element.id+' value="'+element.evenType+'"></option>';
    });
    strFormulaire +='</datalist></div><div class="row"><div class="col-lg-2"><label>Temps :</label></div><div class="col-lg-2"><input type="number" min="0" name="time" id="time" step="1" required></input></div></div>';
    strFormulaire+='<div class="row"><div class="col-lg-2"><label>Choisir le joueur :</label></div><div class="col-lg-2"><input list="joueurs" id="joueur" name="joueur" required></input> <datalist id="joueurs">';
    joueur1 = JSON.parse(data.dataset.joueurs1);
    joueur1.forEach(element => {
        strFormulaire+= '<option id='+element.id+' name='+element.id+' value="'+element.nom+' '+element.prenom+' '+element.numero+'"></option>';
    });
    strFormulaire+='</datalist></div></div><div class="row"><div class="col-lg-3"><input type="submit" value="enregistrer l\'evenement"></input></div></div></div></form></div>';
    document.getElementById("formulaireAera").innerHTML = strFormulaire;
}

function changeJoueurEvenement()
{
    var value = document.getElementById('equipe').value;
    var data = document.getElementById('data');
    equipe1 = JSON.parse(data.dataset.equipe1);
    equipe2 = JSON.parse(data.dataset.equipe2);
    var joueur;
    if (value==equipe1.id) {
        joueur = JSON.parse(data.dataset.joueurs1);
    } else {
        joueur = JSON.parse(data.dataset.joueurs2);
    }
    strFormulaire = "";
    joueur.forEach(element => {
        strFormulaire+= '<option id='+element.id+' name='+element.id+' value="'+element.nom+' '+element.prenom+' '+element.numero+'"></option>';
    });
    document.getElementById('joueurs').innerHTML = strFormulaire;
}

function printDocument() {
        const element = document.getElementById("toPrint");
        html2pdf()
          .from(element)
          .save();
}