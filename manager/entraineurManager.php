<?php   
spl_autoload_register('loadEntity');


class entraineurManager extends model{
    
    public function SelectAll()
    {
        $request = $this->request('SELECT * FROM entraineur');
        $entraineurs = array(); 
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $entraineur = new entraineur($data);
            array_push($entraineurs, $entraineur);
        }
        return $organisateurs;
    }

    public function SelectById($id)
    {
        $param = array( $id);
        $request = $this->request("SELECT * FROM entraineur WHERE identifiant=?", $param);
        $entraineurs = array(); 
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $entraineur = new entraineur($data);
            array_push($entraineurs, $entraineur);
        }
        return $entraineurs[0];
    }

    public function SelectByName($surname,$name)
    {
        $param = array( $surname,$name);
        $request = $this->request("SELECT * FROM entraineur WHERE prenom=? AND nom=?", $param);
        $entraineurs = array(); 
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $entraineur = new entraineur($data);
            array_push($entraineurs, $entraineur);
        }
        if(count($entraineurs)==0){
            return null;
        }
        else{
            return $entraineurs[0];
        }
    }
}

?>