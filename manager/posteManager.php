<?php
spl_autoload_register('loadManager');
spl_autoload_register('loadEntity');

class posteManager extends Model
{
    public function InsertAll(array $arrayPoste)
    {
        foreach ($arrayPoste as $poste) {
            $param = array($poste->getIntitule(),$poste->getEstRemplacant(),$poste->getIdJoueurs(),$poste->getIdFDM());
            $request = $this->request('INSERT INTO poste(intitule,estRemplacant,idJoueurs,idFDM) VALUES (?,?,?,?)', $param);
        }
    }

    public function selectByjoueurIdAndFdmId($idJoueur,$idFdm)
    {
        $param = array($idJoueur,$idFdm);
        $request = $this->request('SELECT * FROM poste WHERE ?=idJoueurs AND ?=idFDM',$param);
        $postes = array(); 
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $poste = new poste($data);
            array_push($postes, $poste);
        }
        return $postes[0];
    }

    public function Update(poste $poste)
    {
        $param = array($poste->getIntitule(),$poste->getEstRemplacant(),$poste->getIdJoueurs(),$poste->getIdFDM(),$poste->getIdentifiant());
        $this->request("UPDATE poste SET intitule=?,estRemplacant=?,idJoueurs=?,idFDM=? WHERE ?=identifiant",$param);
    }
}

?>