<?php
spl_autoload_register('loadManager');
spl_autoload_register('loadEntity');

    class equipeManager extends model{
        public function selectByFDMId($idFDM){
            
            $sql = "SELECT DISTINCT equipe.identifiant, equipe.nom, equipe.ville, equipe.pays from equipe
            join joueurs ON joueurs.idEquipe = equipe.identifiant
            join poste ON poste.idJoueurs = joueurs.identifiant
            Where ? = poste.idFDM";
            $param = array();
            array_push($param, $idFDM);
            $request = $this->request($sql,$param);
            $Tequipe = array();
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $equipe = new equipe($data);
                array_push($Tequipe, $equipe);
            }
            return $Tequipe;
        }

        public function selectByName($name)
        {
            $param = array($name);
            $request = $this->request("SELECT identifiant, nom, ville, pays FROM equipe WHERE ? = nom",$param);
            $Tequipe = Array();
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $equipe = new equipe($data);
                array_push($Tequipe, $equipe);
            }
            return $Tequipe[0];
        }

        public function selectById($id)
        {
            $param = array($id);
            $request = $this->request("SELECT identifiant, nom, ville, pays FROM equipe WHERE ? = identifiant",$param);
            $Tequipe = Array();
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $equipe = new equipe($data);
                array_push($Tequipe, $equipe);
            }
            return $Tequipe[0];
        }

        public function selectAll(){
            $request = $this->request('SELECT identifiant,nom, ville,pays FROM equipe');
            $equipes = array(); 
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $equipe = new equipe($data);
                array_push($equipes, $equipe);
            }
            return $equipes;
        }
    }
?>
