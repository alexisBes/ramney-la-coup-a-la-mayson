<?php

spl_autoload_register('loadManager');
spl_autoload_register('loadEntity');


class arbitrerManager extends Model
{

    public function SelectByID($id)
    {
        $param = array ( $id);
        $request = $this->request('SELECT estPrincipal,idArbitre,idFDM FROM arbitrer WHERE ? = identifiant', $param);
        $arbitres = array(); 
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $arbitre = new arbitrer($data);
            array_push($arbitres, $arbitre);
        }
        return $arbitres;
    }
    
    public function InsertOne(arbitrer $arbitrer)
    {
        $param = array($arbitrer->getEstPrincipal(),$arbitrer->getIdArbitre(),$arbitrer->getIdFDM());
        $request = $this->request('INSERT INTO arbitrer VALUES (?,?,?)', $param);
    }

    public function InsertAll(array $arbitrers)
    {
        foreach ($arbitrers as $arbitrer) {
            $param = array($arbitrer->getEstPrincipal(),$arbitrer->getIdArbitre(),$arbitrer->getIdFDM());
            $request = $this->request('INSERT INTO arbitrer VALUES (?,?,?)', $param);
        }
    }
}
?>