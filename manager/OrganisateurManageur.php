<?php
spl_autoload_register('loadManager');
spl_autoload_register('loadEntity');



class OrganisateurManageur extends Model
{
    public function SelectAll()
    {
        $request = $this->request('SELECT identifiant, mdp FROM organisateur');
        $organisateurs = array(); 
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $orga = new Organisateur($data);
            array_push($organisateurs, $orga);
        }
        return $organisateurs;
    }
    public function Select($name)
    {
        $sql = "SELECT identifiant, mdp FROM organisateur WHERE identifiant=?";
        $param = array();
        array_push($param, $name);
        $request = $this->request($sql, $param);
        $organisateurs = array(); 
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $orga = new Organisateur($data);
            array_push($organisateurs, $orga);
        }
        if(count($organisateurs)==0){
            return null;
        }
        else{
            return $organisateurs[0];
        }
    }
}

?>