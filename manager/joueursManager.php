<?php
spl_autoload_register('loadManager');
spl_autoload_register('loadEntity');
    

    class joueursManager extends model{

        public function selectOneByID($idJoueur){
            $param = array($idJoueur);
            $request = $this->request("SELECT identifiant, nom, prenom, ville, idEquipe, numero from joueurs WHERE ? = identifiant", $param);
            $Tjoueurs = Array();
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $joueur = new joueurs($data);
                array_push($Tjoueurs, $joueur);
            }
            return $Tjoueurs[0];
        }

        public function selectOneByNameAndIDEquipe($name,$surname){            
            $param = array($name,$surname);
            $request = $this->request("SELECT joueurs.* from joueurs WHERE ? = nom AND ? = prenom", $param);
            $Tjoueurs = Array();
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $joueur = new joueurs($data);
                array_push($Tjoueurs, $joueur);
            }
            return $Tjoueurs[0];
        }

        public function selectByPosteId($idPoste){
        $sql = "SELECT DISTINCT joueurs.identifiant, joueurs.nom, joueurs.prenom, joueurs.ville, joueurs.idEquipe, joueurs.numero from joueurs
        join poste ON poste.idJoueurs = joueurs.identifiant
        Where ? = poste.identifiant" ;
        $param = array();
        array_push($param, $idPoste);
        $request = $this->request($sql, $param);
        $Tjoueurs = Array();
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $joueur = new joueurs($data);
            array_push($Tjoueurs, $joueur);
        }
        return $Tjoueurs[0];
        }

        public function selectByEquipeId($idEquipe)
        {
            $param = array($idEquipe);
            $request = $this->request("SELECT identifiant, nom, prenom, ville, idEquipe, numero from joueurs WHERE ? = idEquipe", $param);
            $joueurs = Array();
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $joueur = new joueurs($data);
                array_push($joueurs, $joueur);
            }
            return $joueurs;
        }

        public function selectFirstTeamPlayer($idEquipe,$idFDM)
        {            
            $param = array($idEquipe,$idFDM,"BANC");
            $request = $this->request("SELECT joueurs.* from poste JOIN joueurs ON joueurs.identifiant = poste.idJoueurs WHERE ? = idEquipe AND poste.idFDM = ? AND UPPER(poste.intitule) <> ?", $param);
            $joueurs = Array();
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $joueur = new joueurs($data);
                array_push($joueurs, $joueur);
            }
            return $joueurs;
        }

        public function update(joueurs $joueur){
            $param = array($joueur->getNom(),$joueur->getPrenom(),$joueur->getVille(),$joueur->getIdEquipe(),$joueur->getNumero(),$joueur->getRole(),$joueur->getIdentifiant());
            $this->request("UPDATE joueurs SET nom=?,prenom=?,ville=?,idEquipe=?,numero=?,`role`=? WHERE ?=identifiant",$param);
        }
    }
?>