<?php
spl_autoload_register('loadManager');
spl_autoload_register('loadEntity');


class feuilleDeMatchManager extends model{
    public function selectAll(){
        $sql = "SELECT * FROM feuilledematch ORDER BY dateRencontre";
        $request = $this->request($sql);
        $TFDM = Array();
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        { 
            $FDM = new feuilleDeMatch($data);
            array_push($TFDM, $FDM);
        }
       
        return $TFDM;
    }

    public function selectFDMById($id){
        $sql = "SELECT * FROM feuilleDeMatch WHERE identifiant=?";
        $param = array();
        array_push($param, $id);
        $request = $this->request($sql, $param);
        $TFDM = Array();
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $FDM = new feuilleDeMatch($data);
            array_push($TFDM, $FDM);
        }
        return $TFDM[0];
    }

    public function InsertOne(feuilleDeMatch $fdm)
    {  
        $param = array($fdm->getLieu(),$fdm->getDateRencontre());
        $request = $this->request("INSERT INTO feuilleDeMatch(lieu,dateRencontre) VALUES (?,?)", $param);
        $request = $this->request("SELECT LAST_INSERT_ID()");
        $id = $request->fetch(PDO::FETCH_ASSOC)['LAST_INSERT_ID()'];
        $fdm->setIdentifiant($id);
    }
}

?>