<?php
    spl_autoload_register('loadManager');
    spl_autoload_register('loadEntity');

    class evenementManager extends model{

        public function selectByEvenementId($Id){
            $sql = "SELECT DISTINCT * from evenement Where ? = identifiant" ;
            $param = array();
            array_push($param, $Id);
            $request = $this->request($sql, $param);
            $Tevenement = Array();
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $evenement = new evenement($data);
                array_push($Tevenement, $evenement);
            }
            return $Tevenement[0];
        }

        public function selectAll(){
            $request = $this->request("SELECT * from evenement");
            $Tevenement = Array();
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $evenement = new evenement($data);
                array_push($Tevenement, $evenement);
            }
            return $Tevenement;
        }
    }
?>