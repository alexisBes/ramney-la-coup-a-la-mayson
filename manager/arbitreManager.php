<?php

spl_autoload_register('loadManager');
spl_autoload_register('loadEntity');


class arbitreManager extends Model
{
    public function SelectAll()
    {
        $request = $this->request('SELECT identifiant,nom,prenom,nationalite FROM arbitre');
        $arbitres = array(); 
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $arbitre = new arbitre($data);
            array_push($arbitres, $arbitre);
        }
        return $arbitres;
    }
    
    public function SelectByNAme($surname,$name)
    {
        $param = array ( $surname,$name);
        $request = $this->request('SELECT identifiant,nom,prenom,nationalite FROM arbitre WHERE ? = prenom AND ? = nom', $param);
        $arbitres = array(); 
        while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
        {
            $arbitre = new arbitre($data);
            array_push($arbitres, $arbitre);
        }
        return $arbitres;
    }
    public function insert($nom, $prenom, $nationalite){
        $sql = "INSERT INTO arbitre(nom,prenom,nationalite) VALUES (?,?,?)";
        $param = array();
        array_push($param,$nom);
        array_push($param,$prenom);
        array_push($param,$nationalite);
        $this->request($sql,$param);
    }
}

?>