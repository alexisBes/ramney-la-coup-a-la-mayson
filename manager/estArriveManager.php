<?php
spl_autoload_register('loadManager');
spl_autoload_register('loadEntity');
    

    class estArriveManager extends model{
        public function selectByFDMId($idFDM){
            $sql = "SELECT DISTINCT estArrive.identifiant, estArrive.IdPoste, estArrive.IdEvenement, estArrive.Temps from estArrive
            join poste ON poste.Identifiant = estArrive.IdPoste
            Where ? = poste.idFDM Order By estArrive.Temps" ;
            $param = array();
            array_push($param, $idFDM);
            $request = $this->request($sql, $param);
            $TestArrive = Array();
            while ($data = $request->fetch(PDO::FETCH_ASSOC)) 
            {
                $estArrive = new estArrive($data);
                array_push($TestArrive, $estArrive);
            }
            return $TestArrive;
        }

        public function getbutByIdMatchAndIdEquipe($idMatch, $idEquipe){
            $sql = "SELECT Count(*) From estArrive
            join poste on estArrive.IdPoste = poste.identifiant
            join joueurs on poste.idJoueurs = joueurs.identifiant
            WHERE poste.idFDM = ?
            and joueurs.idEquipe = ?
            and estArrive.idEvenement=1";
            $param = array();
            array_push($param, $idMatch);
            array_push($param, $idEquipe);
            $request = $this->request($sql, $param);
            $TBut =  $request->fetch(PDO::FETCH_ASSOC);
           
            return $TBut['Count(*)'];

        }

        public function insertOne(estarrive $estArrive)
        {
            $param = array($estArrive->getIdPoste(),$estArrive->getIdEvenement(),$estArrive->getTemps());
            $request = $this->request('INSERT INTO estArrive(IdPoste,IdEvenement,Temps) VALUES (?,?,?)', $param);
            $request = $this->request("SELECT LAST_INSERT_ID()");
            $id = $request->fetch(PDO::FETCH_ASSOC)['LAST_INSERT_ID()'];
            $estArrive->setIdentifiant($id);
        }
    }
?>