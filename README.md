# Ramney la coup a la maison
## Configuration
### Developement
Ajouter un fichier dev.ini dans le dossier racine
 * le nom dns de la base de donné,
 * un utilisateur
 * son mot de passe
The file have to looks like this :
```ini
[BD]
info = 'mysql:host=localhost;dbname=name_of_my_database;charset=utf8'
user = ;username
password = ;password
```

### Production
Il faut ajouter un prod.ini. Le prod.ini est lu par defaut

## Architecture
Tous les controller doivent etre dans le dossier controller.
Dans le dossier view, il faut un dossier par controller et un fichier par action.Les fichier sont en PHP.

## Installation

### Placement des fichier
Aller dans le dossier ``` C:\path\to\folder\wamp64\www ``` et copier le dossier a l'interieur.

### Création d'un virtualhost
Pour creer un virtualhost sur wamp, il suffit d'aller a cette addresse : 
http://localhost/add_vhost.php?lang=french  
Apres avoir remplis le formulaire comme indiqué par wamp, enregistre et faire un redémarrage DNS.

### Création et peuplage de la base de donnée
Le fichier ```wamp\projetweb.sql ``` possède la structure de la base de donnée et un jeu de donnée de base.  
Les identifiants et les mots de passe pour les entraineur et les administrateurs sont présents dans le word present sur le dropbox.

### Ajout d'un utilisateur avec un mot de passe
Pour éviter que les mot de passe soit en clair dans la base de donnée, les mots de passse doivent etre haché et le resultat dans la base.  
Le hachage de mot de passe peut etre fait sur ce site : https://phppasswordhash.com  