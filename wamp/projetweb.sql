-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 21 jan. 2021 à 14:19
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `projetweb`
--

-- --------------------------------------------------------

--
-- Structure de la table `arbitre`
--

DROP TABLE IF EXISTS `arbitre`;
CREATE TABLE IF NOT EXISTS `arbitre` (
  `identifiant` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `nationalite` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`identifiant`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `arbitre`
--

INSERT INTO `arbitre` (`identifiant`, `nom`, `prenom`, `nationalite`) VALUES
(1, 'Clérico', 'Antoine', 'France'),
(2, 'boucher', 'Maxime', 'France'),
(3, 'serein', 'Léo', 'belge');

-- --------------------------------------------------------

--
-- Structure de la table `arbitrer`
--

DROP TABLE IF EXISTS `arbitrer`;
CREATE TABLE IF NOT EXISTS `arbitrer` (
  `estPrincipal` int(2) DEFAULT NULL,
  `idArbitre` varchar(5) DEFAULT NULL,
  `idFDM` varchar(5) DEFAULT NULL,
  KEY `idArbitre` (`idArbitre`),
  KEY `idFDM` (`idFDM`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `arbitrer`
--

INSERT INTO `arbitrer` (`estPrincipal`, `idArbitre`, `idFDM`) VALUES
(1, '1', '8'),
(0, '2', '8'),
(0, '3', '8'),
(1, '1', '9'),
(0, '2', '9'),
(0, '3', '9'),
(1, '1', '10'),
(0, '1', '10'),
(0, '1', '10'),
(1, '1', '11'),
(0, '1', '11'),
(0, '1', '11'),
(1, '1', '12'),
(0, '2', '12'),
(0, '3', '12'),
(1, '1', '13'),
(0, '2', '13'),
(0, '3', '13');

-- --------------------------------------------------------

--
-- Structure de la table `entraineur`
--

DROP TABLE IF EXISTS `entraineur`;
CREATE TABLE IF NOT EXISTS `entraineur` (
  `identifiant` int(5) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `estAdjoint` int(2) DEFAULT NULL,
  `idEquipe` varchar(5) DEFAULT NULL,
  `mdp` varchar(255) NOT NULL,
  PRIMARY KEY (`identifiant`),
  KEY `idEquipe` (`idEquipe`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `entraineur`
--

INSERT INTO `entraineur` (`identifiant`, `nom`, `prenom`, `estAdjoint`, `idEquipe`, `mdp`) VALUES
(1, 'lombard', 'hervé', 0, '1', '$2y$10$yGixqFOHaagG3JDISZQQkeQxAXwfNrA4lYK778VzR23WeD4eIjOHq'),
(2, 'Mary', 'bob', NULL, '2', '$2y$10$AqX4iBmmi7uR8JCO/q82Ye0aXsnhk2dA60yK43yCCd8tSNWqJm.Vi'),
(3, 'jacque', 'jean', NULL, '3', '$2y$10$AqX4iBmmi7uR8JCO/q82Ye0aXsnhk2dA60yK43yCCd8tSNWqJm.Vi'),
(4, 'sassier', 'simon', NULL, '4', '$2y$10$AqX4iBmmi7uR8JCO/q82Ye0aXsnhk2dA60yK43yCCd8tSNWqJm.Vi');

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

DROP TABLE IF EXISTS `equipe`;
CREATE TABLE IF NOT EXISTS `equipe` (
  `identifiant` int(5) NOT NULL AUTO_INCREMENT,
  `nom` varchar(150) DEFAULT NULL,
  `ville` varchar(100) DEFAULT NULL,
  `pays` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`identifiant`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `equipe`
--

INSERT INTO `equipe` (`identifiant`, `nom`, `ville`, `pays`) VALUES
(1, 'PSG', 'v1', 'p1'),
(2, 'OM', 'v2', 'p2'),
(3, 'OL', 'v3', 'p3'),
(4, 'ASSE', 'v4', 'p4');

-- --------------------------------------------------------

--
-- Structure de la table `estarrive`
--

DROP TABLE IF EXISTS `estarrive`;
CREATE TABLE IF NOT EXISTS `estarrive` (
  `identifiant` int(11) NOT NULL AUTO_INCREMENT,
  `IdPoste` int(11) NOT NULL,
  `IdEvenement` int(11) NOT NULL,
  `Temps` int(11) NOT NULL,
  PRIMARY KEY (`identifiant`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `estarrive`
--

INSERT INTO `estarrive` (`identifiant`, `IdPoste`, `IdEvenement`, `Temps`) VALUES
(1, 1, 1, 24),
(2, 3, 2, 33),
(3, 3, 1, 50),
(4, 28, 1, 55),
(5, 249, 1, 31),
(6, 266, 2, 70),
(7, 246, 3, 70),
(8, 252, 1, 72),
(9, 248, 1, 82);

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

DROP TABLE IF EXISTS `evenement`;
CREATE TABLE IF NOT EXISTS `evenement` (
  `identifiant` int(5) NOT NULL AUTO_INCREMENT,
  `evenType` varchar(13) DEFAULT NULL,
  PRIMARY KEY (`identifiant`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `evenement`
--

INSERT INTO `evenement` (`identifiant`, `evenType`) VALUES
(1, 'but'),
(2, 'blessure'),
(3, 'carton jaune'),
(4, 'carton rouge'),
(5, 'entrée'),
(6, 'sortie');

-- --------------------------------------------------------

--
-- Structure de la table `feuilledematch`
--

DROP TABLE IF EXISTS `feuilledematch`;
CREATE TABLE IF NOT EXISTS `feuilledematch` (
  `identifiant` int(5) NOT NULL AUTO_INCREMENT,
  `lieu` varchar(100) DEFAULT NULL,
  `dateRencontre` date DEFAULT NULL,
  PRIMARY KEY (`identifiant`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `feuilledematch`
--

INSERT INTO `feuilledematch` (`identifiant`, `lieu`, `dateRencontre`) VALUES
(1, 'Ville 1', '2021-01-17'),
(2, 'Ville 2', '2021-01-28'),
(3, 'ville 3', '2021-01-29'),
(4, 'ville 4', '2021-01-30'),
(5, 'Ville 5', '2021-02-01'),
(6, 'Ville 6', '2021-02-03'),
(8, 'Pau', '2021-01-20'),
(9, 'Tarbes', '2021-01-28'),
(10, 'paris', '2021-02-22'),
(11, 'Lyon', '2021-03-03'),
(12, 'Lyon', '2021-01-23'),
(13, 'paris', '2021-01-22');

-- --------------------------------------------------------

--
-- Structure de la table `joueurs`
--

DROP TABLE IF EXISTS `joueurs`;
CREATE TABLE IF NOT EXISTS `joueurs` (
  `identifiant` int(5) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `ville` varchar(100) DEFAULT NULL,
  `idEquipe` int(5) DEFAULT NULL,
  `numero` int(11) NOT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`identifiant`),
  KEY `idEquipe` (`idEquipe`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `joueurs`
--

INSERT INTO `joueurs` (`identifiant`, `nom`, `prenom`, `ville`, `idEquipe`, `numero`, `role`) VALUES
(1, 'seymour', 'parent', 'v1', 1, 1, ''),
(2, 'Dubost', 'Abeau', 'v1', 1, 2, ''),
(3, 'Allaire', 'Félix', 'v1', 1, 3, ''),
(4, 'Ancel', 'Jérémie', 'v1', 1, 4, ''),
(5, 'Doisneau', 'Roland', 'v1', 1, 5, ''),
(6, 'Cartier', 'Félix', 'v1', 1, 6, ''),
(7, 'Blondeau', 'Frank', 'v1', 1, 7, ''),
(8, 'Lajoie', 'Gauthier', 'v1', 1, 8, ''),
(9, 'Bruneau', 'Joseph', 'v1', 1, 9, ''),
(10, 'Courbet', 'Dominique', 'v1', 1, 10, ''),
(11, 'Sardou', 'Paulin', 'v1', 1, 11, ''),
(12, 'Barbier', 'Manuel', 'v1', 1, 12, ''),
(13, 'Bazalgette', 'Tibault', 'v1', 1, 13, ''),
(14, 'Colbert', 'Lionel', 'v1', 1, 14, ''),
(15, 'De Varley', 'Cédric', 'v1', 1, 15, ''),
(16, 'Alex', 'Berengar', 'v1', 1, 16, ''),
(17, 'Bonhomme', 'Grégory', 'v1', 1, 17, ''),
(18, 'kaplan', 'Frédéric', 'v1', 1, 18, ''),
(19, 'Stanislas', 'Sartre', 'v1', 1, 19, ''),
(20, 'Clair', 'Marius', 'v1', 1, 20, ''),
(21, 'Vernier', 'Antoine', 'v2', 2, 1, 'capitaine'),
(22, 'Beaugendre', 'Antoine', 'v2', 2, 2, 'adjoint'),
(23, 'Josué', 'Charrier', 'v2', 2, 3, ''),
(24, 'Auvray', 'Emilien', 'v2', 2, 4, ''),
(25, 'Ouvrard', 'Gaby', 'v2', 2, 5, ''),
(26, 'Cerfbeer', 'Tibault', 'v2', 2, 6, ''),
(27, 'Serre', 'Alebin', 'v2', 2, 7, 'adjoint'),
(28, 'Bethune', 'Dylan', 'v2', 2, 8, ''),
(29, 'Bourgeois', 'Garspard', 'v2', 2, 9, ''),
(30, 'Naudé', 'Romuald', 'v2', 2, 10, ''),
(31, 'Bassot', 'Emeric', 'v2', 2, 11, ''),
(32, 'Courtial', 'Antoine', 'v2', 2, 12, ''),
(33, 'Delisle', 'Christopher', 'v2', 2, 13, ''),
(34, 'Guilloux', 'Théo', 'v2', 2, 14, ''),
(35, 'Baugé', 'Jean-Pascal', 'v2', 2, 15, ''),
(36, 'Plouffe', 'Vivien', 'v2', 2, 16, ''),
(37, 'Soyer', 'Djeferson', 'v2', 2, 17, ''),
(38, 'Charpentier', 'Napoléon', 'v2', 2, 18, ''),
(39, 'Bourgeois', 'Basile', 'v2', 2, 19, ''),
(40, 'Compere', 'Louis', 'v2', 2, 20, ''),
(41, 'Amboise', 'Frederic', 'v3', 3, 1, ''),
(42, 'Poulin', 'Bruno', 'v3', 3, 2, 'capitaine'),
(43, 'Charbonnier', 'Barthélemy', 'v3', 3, 3, 'adjoint'),
(44, 'Asselineau', 'Jonathan', 'v3', 3, 4, ''),
(45, 'Christophe', 'Rochette', 'v3', 3, 5, ''),
(46, 'Alard', 'Frank', 'v3', 3, 6, ''),
(47, 'Cerfbeef', 'Enzo', 'v3', 3, 7, ''),
(48, 'Gauthier', 'Jean-Francois', 'v3', 3, 8, ''),
(49, 'Mossé', 'Auguste', 'v3', 3, 9, ''),
(50, 'Amaury', 'Lortie', 'v3', 3, 10, ''),
(51, 'Bureau', 'Thierry', 'v3', 3, 11, ''),
(52, 'Dumont', 'Jérémie', 'v3', 3, 12, ''),
(53, 'Mesny', 'Guy', 'v3', 3, 13, ''),
(54, 'Carpentier', 'Lilian', 'v3', 3, 14, ''),
(55, 'Vigouroux', 'Etienne', 'v3', 3, 15, ''),
(56, 'Edige', 'Chaney', 'v3', 3, 16, ''),
(57, 'Du Toit', 'Mathis', 'v3', 3, 17, ''),
(58, 'LongChambon', 'Ange', 'v3', 3, 18, ''),
(59, 'Gagnon', 'Tristan', 'v3', 3, 19, ''),
(60, 'Frère', 'Jean-Noel', 'v3', 3, 20, ''),
(61, 'Azais', 'Théophile', 'v4', 4, 1, ''),
(62, 'Bozonnet', 'Alain', 'v4', 4, 2, ''),
(63, 'Lièvremont', 'Marcel', 'v4', 4, 3, ''),
(64, 'Jean-Francois', 'Berlioz', 'v4', 4, 4, ''),
(65, 'Grégory', 'Brunelle', 'v4', 4, 5, ''),
(66, 'Dufresne', 'Vivien', 'v4', 4, 6, ''),
(67, 'Roatta', 'Bastien', 'v4', 4, 7, ''),
(68, 'Marcel', 'Favre', 'v4', 4, 8, ''),
(69, 'Dubois', 'Florentin', 'v4', 4, 9, ''),
(70, 'Loup', 'William', 'v4', 4, 10, ''),
(71, 'Dutoit', 'Jacob', 'v4', 4, 11, ''),
(72, 'Dufresne', 'Luc', 'v4', 4, 12, ''),
(73, 'Eric', 'Boucher', 'v4', 4, 13, ''),
(74, 'Boudreaux', 'Pierre-Marie', 'v4', 4, 14, ''),
(75, 'Robiquet', 'Roland', 'v4', 4, 15, ''),
(76, 'Bassot', 'Nathan', 'v4', 4, 16, ''),
(77, 'Gaudreau', 'Guy', 'v4', 4, 17, ''),
(78, 'Girault', 'Noel', 'v4', 4, 18, ''),
(79, 'Bombelles', 'Désiré', 'v4', 4, 19, ''),
(80, 'Bonhomme', 'Roland', 'v4', 4, 20, '');

-- --------------------------------------------------------

--
-- Structure de la table `organisateur`
--

DROP TABLE IF EXISTS `organisateur`;
CREATE TABLE IF NOT EXISTS `organisateur` (
  `identifiant` varchar(50) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  PRIMARY KEY (`identifiant`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `organisateur`
--

INSERT INTO `organisateur` (`identifiant`, `mdp`) VALUES
('alexis', ''),
('corentin', '$2y$10$FhaG7kJzH9KmvApcSIJkL.6MW9J.JGsQUSpj8mS4RL8VTjJ3Gtije');

-- --------------------------------------------------------

--
-- Structure de la table `poste`
--

DROP TABLE IF EXISTS `poste`;
CREATE TABLE IF NOT EXISTS `poste` (
  `identifiant` int(5) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(20) DEFAULT NULL,
  `estRemplacant` int(2) DEFAULT NULL,
  `idJoueurs` int(5) NOT NULL,
  `idFDM` int(5) NOT NULL,
  PRIMARY KEY (`identifiant`)
) ENGINE=MyISAM AUTO_INCREMENT=481 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `poste`
--

INSERT INTO `poste` (`identifiant`, `intitule`, `estRemplacant`, `idJoueurs`, `idFDM`) VALUES
(1, 'banc', 0, 1, 1),
(2, 'banc', 0, 2, 1),
(3, 'banc', 0, 3, 1),
(4, 'banc', 0, 4, 1),
(5, 'banc', 0, 5, 1),
(6, 'banc', 0, 6, 1),
(7, 'banc', 0, 7, 1),
(8, 'banc', 0, 8, 1),
(9, 'banc', 0, 9, 1),
(10, 'banc', 0, 10, 1),
(11, 'banc', 0, 11, 1),
(12, 'banc', 0, 12, 1),
(13, 'banc', 0, 13, 1),
(14, 'banc', 0, 14, 1),
(15, 'banc', 0, 15, 1),
(16, 'banc', 0, 16, 1),
(17, 'banc', 0, 17, 1),
(18, 'banc', 0, 18, 1),
(19, 'banc', 0, 19, 1),
(20, 'banc', 0, 20, 1),
(21, 'banc', 0, 21, 1),
(22, 'banc', 0, 22, 1),
(23, 'banc', 0, 23, 1),
(24, 'banc', 0, 24, 1),
(25, 'banc', 0, 25, 1),
(26, 'banc', 0, 26, 1),
(27, 'banc', 0, 27, 1),
(28, 'banc', 0, 28, 1),
(29, 'banc', 0, 29, 1),
(30, 'banc', 0, 30, 1),
(31, 'banc', 0, 31, 1),
(32, 'banc', 0, 32, 1),
(33, 'banc', 0, 33, 1),
(34, 'banc', 0, 34, 1),
(35, 'banc', 0, 35, 1),
(36, 'banc', 0, 36, 1),
(37, 'banc', 0, 37, 1),
(38, 'banc', 0, 38, 1),
(39, 'banc', 0, 39, 1),
(40, 'banc', 0, 40, 1),
(41, 'ailier_gauche', 0, 1, 2),
(42, 'avant_centre', 0, 2, 2),
(43, 'ailier_droit', 0, 3, 2),
(44, 'milieu_gauche', 0, 4, 2),
(45, 'milieu_offensif', 0, 5, 2),
(46, 'milieu_centre', 0, 6, 2),
(47, 'milieu_defensif', 0, 7, 2),
(48, 'arriere_gauche', 0, 8, 2),
(49, 'defenseur_central', 0, 9, 2),
(50, 'milieu_offensif', 0, 10, 2),
(51, 'Gardien', 0, 11, 2),
(52, 'banc', 0, 12, 2),
(53, 'banc', 0, 13, 2),
(54, 'banc', 0, 14, 2),
(55, 'banc', 0, 15, 2),
(56, 'banc', 0, 16, 2),
(57, 'banc', 0, 17, 2),
(58, 'banc', 0, 18, 2),
(59, 'banc', 0, 19, 2),
(60, 'banc', 0, 20, 2),
(61, 'banc', 0, 41, 2),
(62, 'banc', 0, 42, 2),
(63, 'banc', 0, 43, 2),
(64, 'banc', 0, 44, 2),
(65, 'banc', 0, 45, 2),
(66, 'banc', 0, 46, 2),
(67, 'banc', 0, 47, 2),
(68, 'banc', 0, 48, 2),
(69, 'banc', 0, 49, 2),
(70, 'banc', 0, 50, 2),
(71, 'banc', 0, 51, 2),
(72, 'banc', 0, 52, 2),
(73, 'banc', 0, 53, 2),
(74, 'banc', 0, 54, 2),
(75, 'banc', 0, 55, 2),
(76, 'banc', 0, 56, 2),
(77, 'banc', 0, 57, 2),
(78, 'banc', 0, 58, 2),
(79, 'banc', 0, 59, 2),
(80, 'banc', 0, 60, 2),
(81, 'banc', 0, 1, 3),
(82, 'banc', 0, 2, 3),
(83, 'banc', 0, 3, 3),
(84, 'banc', 0, 4, 3),
(85, 'banc', 0, 5, 3),
(86, 'banc', 0, 6, 3),
(87, 'banc', 0, 7, 3),
(88, 'banc', 0, 8, 3),
(89, 'banc', 0, 9, 3),
(90, 'banc', 0, 10, 3),
(91, 'banc', 0, 11, 3),
(92, 'banc', 0, 12, 3),
(93, 'banc', 0, 13, 3),
(94, 'banc', 0, 14, 3),
(95, 'banc', 0, 15, 3),
(96, 'banc', 0, 16, 3),
(97, 'banc', 0, 17, 3),
(98, 'banc', 0, 18, 3),
(99, 'banc', 0, 19, 3),
(100, 'banc', 0, 20, 3),
(101, 'banc', 0, 61, 3),
(102, 'banc', 0, 62, 3),
(103, 'banc', 0, 63, 3),
(104, 'banc', 0, 64, 3),
(105, 'banc', 0, 65, 3),
(106, 'banc', 0, 66, 3),
(107, 'banc', 0, 67, 3),
(108, 'banc', 0, 68, 2),
(109, 'banc', 0, 69, 3),
(110, 'banc', 0, 70, 3),
(111, 'banc', 0, 71, 3),
(112, 'banc', 0, 72, 3),
(113, 'banc', 0, 73, 3),
(114, 'banc', 0, 74, 3),
(115, 'banc', 0, 75, 3),
(116, 'banc', 0, 76, 3),
(117, 'banc', 0, 77, 3),
(118, 'banc', 0, 78, 3),
(119, 'banc', 0, 79, 3),
(120, 'banc', 0, 80, 3),
(121, 'banc', 0, 21, 5),
(122, 'banc', 0, 22, 5),
(123, 'banc', 0, 23, 5),
(124, 'banc', 0, 24, 5),
(125, 'banc', 0, 25, 5),
(126, 'banc', 0, 26, 5),
(127, 'banc', 0, 27, 5),
(128, 'banc', 0, 28, 5),
(129, 'banc', 0, 29, 5),
(130, 'banc', 0, 30, 5),
(131, 'banc', 0, 31, 5),
(132, 'banc', 0, 32, 5),
(133, 'banc', 0, 33, 5),
(134, 'banc', 0, 34, 5),
(135, 'banc', 0, 35, 5),
(136, 'banc', 0, 36, 5),
(137, 'banc', 0, 37, 5),
(138, 'banc', 0, 38, 5),
(139, 'banc', 0, 39, 5),
(140, 'banc', 0, 40, 5),
(141, 'banc', 0, 61, 5),
(142, 'banc', 0, 62, 5),
(143, 'banc', 0, 63, 5),
(144, 'banc', 0, 64, 5),
(145, 'banc', 0, 65, 5),
(146, 'banc', 0, 66, 5),
(147, 'banc', 0, 67, 5),
(148, 'banc', 0, 68, 5),
(149, 'banc', 0, 69, 5),
(150, 'banc', 0, 70, 5),
(151, 'banc', 0, 71, 5),
(152, 'banc', 0, 72, 5),
(153, 'banc', 0, 73, 5),
(154, 'banc', 0, 74, 5),
(155, 'banc', 0, 75, 5),
(156, 'banc', 0, 76, 5),
(157, 'banc', 0, 77, 5),
(158, 'banc', 0, 78, 5),
(159, 'banc', 0, 79, 5),
(160, 'banc', 0, 80, 5),
(161, 'banc', 0, 21, 4),
(162, 'banc', 0, 22, 4),
(163, 'banc', 0, 23, 4),
(164, 'banc', 0, 24, 4),
(165, 'banc', 0, 25, 4),
(166, 'banc', 0, 26, 4),
(167, 'banc', 0, 27, 4),
(168, 'banc', 0, 28, 4),
(169, 'banc', 0, 29, 4),
(170, 'banc', 0, 30, 4),
(171, 'banc', 0, 31, 4),
(172, 'banc', 0, 32, 4),
(173, 'banc', 0, 33, 4),
(174, 'banc', 0, 34, 4),
(175, 'banc', 0, 35, 4),
(176, 'banc', 0, 36, 4),
(177, 'banc', 0, 37, 4),
(178, 'banc', 0, 38, 4),
(179, 'banc', 0, 39, 4),
(180, 'banc', 0, 40, 4),
(181, 'banc', 0, 41, 4),
(182, 'banc', 0, 42, 4),
(183, 'banc', 0, 43, 4),
(184, 'banc', 0, 44, 4),
(185, 'banc', 0, 45, 4),
(186, 'banc', 0, 46, 4),
(187, 'banc', 0, 47, 4),
(188, 'banc', 0, 48, 4),
(189, 'banc', 0, 49, 4),
(190, 'banc', 0, 50, 4),
(191, 'banc', 0, 51, 4),
(192, 'banc', 0, 52, 4),
(193, 'banc', 0, 53, 4),
(194, 'banc', 0, 54, 4),
(195, 'banc', 0, 55, 4),
(196, 'banc', 0, 56, 4),
(197, 'banc', 0, 57, 4),
(198, 'banc', 0, 58, 4),
(199, 'banc', 0, 59, 4),
(200, 'banc', 0, 60, 4),
(201, 'banc', 0, 61, 6),
(202, 'banc', 0, 62, 6),
(203, 'banc', 0, 63, 6),
(204, 'banc', 0, 64, 6),
(205, 'banc', 0, 65, 6),
(206, 'banc', 0, 66, 6),
(207, 'banc', 0, 67, 6),
(208, 'banc', 0, 68, 6),
(209, 'banc', 0, 69, 6),
(210, 'banc', 0, 70, 6),
(211, 'banc', 0, 71, 6),
(212, 'banc', 0, 72, 6),
(213, 'banc', 0, 73, 6),
(214, 'banc', 0, 74, 6),
(215, 'banc', 0, 75, 6),
(216, 'banc', 0, 76, 6),
(217, 'banc', 0, 77, 6),
(218, 'banc', 0, 78, 6),
(219, 'banc', 0, 79, 6),
(220, 'banc', 0, 80, 6),
(221, 'banc', 0, 41, 6),
(222, 'banc', 0, 42, 6),
(223, 'banc', 0, 43, 6),
(224, 'banc', 0, 44, 6),
(225, 'banc', 0, 45, 6),
(226, 'banc', 0, 46, 6),
(227, 'banc', 0, 47, 6),
(228, 'banc', 0, 48, 6),
(229, 'banc', 0, 49, 6),
(230, 'banc', 0, 50, 6),
(231, 'banc', 0, 51, 6),
(232, 'banc', 0, 52, 6),
(233, 'banc', 0, 53, 6),
(234, 'banc', 0, 54, 6),
(235, 'banc', 0, 55, 6),
(236, 'banc', 0, 56, 6),
(237, 'banc', 0, 57, 6),
(238, 'banc', 0, 58, 6),
(239, 'banc', 0, 59, 6),
(240, 'banc', 0, 60, 6),
(241, 'Gardien', 0, 21, 8),
(242, 'arriere_gauche', 0, 22, 8),
(243, 'arriere_droit', 0, 23, 8),
(244, 'milieu_gauche', 0, 24, 8),
(245, 'milieu_defensif', 0, 25, 8),
(246, 'defenseur_central', 0, 26, 8),
(247, 'milieu_centre', 0, 27, 8),
(248, 'milieu_droit', 0, 28, 8),
(249, 'milieu_offensif', 0, 29, 8),
(250, 'ailier_gauche', 0, 30, 8),
(251, 'ailier_droit', 0, 31, 8),
(252, 'remplacant', 0, 32, 8),
(253, 'remplacant', 0, 33, 8),
(254, 'remplacant', 0, 34, 8),
(255, 'banc', 1, 35, 8),
(256, 'banc', 1, 36, 8),
(257, 'banc', 1, 37, 8),
(258, 'banc', 1, 38, 8),
(259, 'banc', 1, 39, 8),
(260, 'defenseur_central', 0, 40, 8),
(261, 'Gardien', 0, 41, 8),
(262, 'arriere_gauche', 0, 42, 8),
(263, 'defenseur_central', 0, 43, 8),
(264, 'arriere_droit', 0, 44, 8),
(265, 'milieu_gauche', 0, 45, 8),
(266, 'milieu_centre', 0, 46, 8),
(267, 'milieu_defensif', 0, 47, 8),
(268, 'milieu_droit', 0, 48, 8),
(269, 'ailier_gauche', 0, 49, 8),
(270, 'milieu_offensif', 0, 50, 8),
(271, 'ailier_droit', 0, 51, 8),
(272, 'remplacant', 0, 52, 8),
(273, 'remplacant', 0, 53, 8),
(274, 'remplacant', 0, 54, 8),
(275, 'banc', 1, 55, 8),
(276, 'banc', 1, 56, 8),
(277, 'banc', 1, 57, 8),
(278, 'banc', 1, 58, 8),
(279, 'banc', 1, 59, 8),
(280, 'banc', 1, 60, 8),
(281, 'banc', 1, 61, 9),
(282, 'banc', 1, 62, 9),
(283, 'banc', 1, 63, 9),
(284, 'banc', 1, 64, 9),
(285, 'banc', 1, 65, 9),
(286, 'banc', 1, 66, 9),
(287, 'banc', 1, 67, 9),
(288, 'banc', 1, 68, 9),
(289, 'banc', 1, 69, 9),
(290, 'banc', 1, 70, 9),
(291, 'banc', 1, 71, 9),
(292, 'banc', 1, 72, 9),
(293, 'banc', 1, 73, 9),
(294, 'banc', 1, 74, 9),
(295, 'banc', 1, 75, 9),
(296, 'banc', 1, 76, 9),
(297, 'banc', 1, 77, 9),
(298, 'banc', 1, 78, 9),
(299, 'banc', 1, 79, 9),
(300, 'banc', 1, 80, 9),
(301, 'banc', 1, 1, 9),
(302, 'banc', 1, 2, 9),
(303, 'banc', 1, 3, 9),
(304, 'banc', 1, 4, 9),
(305, 'banc', 1, 5, 9),
(306, 'banc', 1, 6, 9),
(307, 'banc', 1, 7, 9),
(308, 'banc', 1, 8, 9),
(309, 'banc', 1, 9, 9),
(310, 'banc', 1, 10, 9),
(311, 'banc', 1, 11, 9),
(312, 'banc', 1, 12, 9),
(313, 'banc', 1, 13, 9),
(314, 'banc', 1, 14, 9),
(315, 'banc', 1, 15, 9),
(316, 'banc', 1, 16, 9),
(317, 'banc', 1, 17, 9),
(318, 'banc', 1, 18, 9),
(319, 'banc', 1, 19, 9),
(320, 'banc', 1, 20, 9),
(321, 'banc', 1, 1, 10),
(322, 'banc', 1, 2, 10),
(323, 'banc', 1, 3, 10),
(324, 'banc', 1, 4, 10),
(325, 'banc', 1, 5, 10),
(326, 'banc', 1, 6, 10),
(327, 'banc', 1, 7, 10),
(328, 'banc', 1, 8, 10),
(329, 'banc', 1, 9, 10),
(330, 'banc', 1, 10, 10),
(331, 'banc', 1, 11, 10),
(332, 'banc', 1, 12, 10),
(333, 'banc', 1, 13, 10),
(334, 'banc', 1, 14, 10),
(335, 'banc', 1, 15, 10),
(336, 'banc', 1, 16, 10),
(337, 'banc', 1, 17, 10),
(338, 'banc', 1, 18, 10),
(339, 'banc', 1, 19, 10),
(340, 'banc', 1, 20, 10),
(341, 'banc', 1, 21, 10),
(342, 'banc', 1, 22, 10),
(343, 'banc', 1, 23, 10),
(344, 'banc', 1, 24, 10),
(345, 'banc', 1, 25, 10),
(346, 'banc', 1, 26, 10),
(347, 'banc', 1, 27, 10),
(348, 'banc', 1, 28, 10),
(349, 'banc', 1, 29, 10),
(350, 'banc', 1, 30, 10),
(351, 'banc', 1, 31, 10),
(352, 'banc', 1, 32, 10),
(353, 'banc', 1, 33, 10),
(354, 'banc', 1, 34, 10),
(355, 'banc', 1, 35, 10),
(356, 'banc', 1, 36, 10),
(357, 'banc', 1, 37, 10),
(358, 'banc', 1, 38, 10),
(359, 'banc', 1, 39, 10),
(360, 'banc', 1, 40, 10),
(361, 'banc', 1, 41, 11),
(362, 'banc', 1, 42, 11),
(363, 'banc', 1, 43, 11),
(364, 'banc', 1, 44, 11),
(365, 'banc', 1, 45, 11),
(366, 'banc', 1, 46, 11),
(367, 'banc', 1, 47, 11),
(368, 'banc', 1, 48, 11),
(369, 'banc', 1, 49, 11),
(370, 'banc', 1, 50, 11),
(371, 'banc', 1, 51, 11),
(372, 'banc', 1, 52, 11),
(373, 'banc', 1, 53, 11),
(374, 'banc', 1, 54, 11),
(375, 'banc', 1, 55, 11),
(376, 'banc', 1, 56, 11),
(377, 'banc', 1, 57, 11),
(378, 'banc', 1, 58, 11),
(379, 'banc', 1, 59, 11),
(380, 'banc', 1, 60, 11),
(381, 'banc', 1, 1, 11),
(382, 'banc', 1, 2, 11),
(383, 'banc', 1, 3, 11),
(384, 'banc', 1, 4, 11),
(385, 'banc', 1, 5, 11),
(386, 'banc', 1, 6, 11),
(387, 'banc', 1, 7, 11),
(388, 'banc', 1, 8, 11),
(389, 'banc', 1, 9, 11),
(390, 'banc', 1, 10, 11),
(391, 'banc', 1, 11, 11),
(392, 'banc', 1, 12, 11),
(393, 'banc', 1, 13, 11),
(394, 'banc', 1, 14, 11),
(395, 'banc', 1, 15, 11),
(396, 'banc', 1, 16, 11),
(397, 'banc', 1, 17, 11),
(398, 'banc', 1, 18, 11),
(399, 'banc', 1, 19, 11),
(400, 'banc', 1, 20, 11),
(401, 'banc', 1, 41, 12),
(402, 'banc', 1, 42, 12),
(403, 'banc', 1, 43, 12),
(404, 'banc', 1, 44, 12),
(405, 'banc', 1, 45, 12),
(406, 'banc', 1, 46, 12),
(407, 'banc', 1, 47, 12),
(408, 'banc', 1, 48, 12),
(409, 'banc', 1, 49, 12),
(410, 'banc', 1, 50, 12),
(411, 'banc', 1, 51, 12),
(412, 'banc', 1, 52, 12),
(413, 'banc', 1, 53, 12),
(414, 'banc', 1, 54, 12),
(415, 'banc', 1, 55, 12),
(416, 'banc', 1, 56, 12),
(417, 'banc', 1, 57, 12),
(418, 'banc', 1, 58, 12),
(419, 'banc', 1, 59, 12),
(420, 'banc', 1, 60, 12),
(421, 'banc', 1, 21, 12),
(422, 'defenseur_central', 0, 22, 12),
(423, 'arriere_gauche', 0, 23, 12),
(424, 'arriere_droit', 0, 24, 12),
(425, 'Gardien', 0, 25, 12),
(426, 'milieu_gauche', 0, 26, 12),
(427, 'milieu_centre', 0, 27, 12),
(428, 'milieu_defensif', 0, 28, 12),
(429, 'milieu_droit', 0, 29, 12),
(430, 'ailier_gauche', 0, 30, 12),
(431, 'ailier_droit', 0, 31, 12),
(432, 'banc', 1, 32, 12),
(433, 'remplacant', 0, 33, 12),
(434, 'remplacant', 0, 34, 12),
(435, 'remplacant', 0, 35, 12),
(436, 'milieu_offensif', 0, 36, 12),
(437, 'banc', 1, 37, 12),
(438, 'banc', 1, 38, 12),
(439, 'banc', 1, 39, 12),
(440, 'banc', 1, 40, 12),
(441, 'banc', 1, 1, 13),
(442, 'banc', 1, 2, 13),
(443, 'banc', 1, 3, 13),
(444, 'banc', 1, 4, 13),
(445, 'banc', 1, 5, 13),
(446, 'banc', 1, 6, 13),
(447, 'banc', 1, 7, 13),
(448, 'banc', 1, 8, 13),
(449, 'banc', 1, 9, 13),
(450, 'banc', 1, 10, 13),
(451, 'banc', 1, 11, 13),
(452, 'banc', 1, 12, 13),
(453, 'banc', 1, 13, 13),
(454, 'banc', 1, 14, 13),
(455, 'banc', 1, 15, 13),
(456, 'banc', 1, 16, 13),
(457, 'banc', 1, 17, 13),
(458, 'banc', 1, 18, 13),
(459, 'banc', 1, 19, 13),
(460, 'banc', 1, 20, 13),
(461, 'banc', 1, 21, 13),
(462, 'banc', 1, 22, 13),
(463, 'banc', 1, 23, 13),
(464, 'banc', 1, 24, 13),
(465, 'banc', 1, 25, 13),
(466, 'banc', 1, 26, 13),
(467, 'banc', 1, 27, 13),
(468, 'banc', 1, 28, 13),
(469, 'banc', 1, 29, 13),
(470, 'banc', 1, 30, 13),
(471, 'banc', 1, 31, 13),
(472, 'banc', 1, 32, 13),
(473, 'banc', 1, 33, 13),
(474, 'banc', 1, 34, 13),
(475, 'banc', 1, 35, 13),
(476, 'banc', 1, 36, 13),
(477, 'banc', 1, 37, 13),
(478, 'banc', 1, 38, 13),
(479, 'banc', 1, 39, 13),
(480, 'banc', 1, 40, 13);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
