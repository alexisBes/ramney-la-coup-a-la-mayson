<?php
class Config  
{
    private static $parameter;
    public static function get($nom,$defaultValue = null){
        if(isset(self::getParameter()[$nom])){
            $value = self::getParameter()[$nom];
            //echo "hello1";
        }else {
            $value = $defaultValue;
            //echo "hello2";
        }
        return $value;
    }

    private static function getParameter(){

        if(self::$parameter == null){
            $file = "prod.ini";
            if (!file_exists($file)) {
                $file = "dev.ini";
                //echo "hello3";
            }
            if (!file_exists($file)) {
                throw new Exception("No configuration file found");
                //echo "hello4";
            }else {
                self::$parameter = parse_ini_file($file);
                //var_dump(self::$parameter);
                //echo "hello5";
            }
        }
        return self::$parameter;
    }
}

?>