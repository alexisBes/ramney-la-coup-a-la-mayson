  
<?php $this->title = "Index"; ?>

<?php $Data;
   
    $nbMatch = count($Data);
    ?>
<?php for($i=0; $i<$nbMatch;$i++):?>
<div id="encadreMatch" class="container">
    <?php if($Data[$i]->getDateRencontre()<date('Y-m-d')): ?>
        <a href="index.php?controller=DetailMatchPasse&id=<?=$Data[$i]->getIdentifiant()?>" id="linkInDark">
    <?php elseif($role == "organisateur" || $role == "entraineur"): ?>
        <a href="index.php?controller=CreationDeMatch&action=completerFeuilleDeMatch&id=<?=$Data[$i]->getIdentifiant()?>" id="linkInDark">
        <?php else: ?>
            <div id="linkInDark">
    <?php endif;?>
            <div  class="col-lg-2" id="infosEncadre">
                <label id="ContenuEncadre"><?=$Data[$i]->getEquipe1()->getNom()?>
                <img src="asset/img/equipe/<?=$Data[$i]->getEquipe1()->getNom()?>.png" id="logoEquipe" />
            </div>
            <div class="col-lg-8" id="infosEncadre">
                <div><label id="ContenuEncadre"><?=$Data[$i]->getDateRencontre()?></label></div>
                <div><label id="ContenuEncadre"><?=$Data[$i]->getLieu()?></label></div>
                <?php
                    if($Data[$i]->getDateRencontre()<date('Y-m-d')):
                ?>
                <div><label id="ContenuEncadreScore"><?=$Data[$i]->getButEquipe1()?>-<?=$Data[$i]->getButEquipe2()?></label></div>
                <?php endif;?>
            </div>
                    <div class="col-lg-2" id="infosEncadre">
                        <label id="ContenuEncadre"><?=$Data[$i]->getEquipe2()->getNom()?></label>
                        <img src="asset/img/equipe/<?=$Data[$i]->getEquipe2()->getNom()?>.png" id="logoEquipe" />
                    </div>
    <?php if($Data[$i]->getDateRencontre()<date('Y-m-d')||$role == "organisateur" || $role == "entraineur"): ?>
        </a>
    <?php else: ?>
        </div>
    <?php endif;?>
</div>

        
<?php endfor;?>
