<?php $this->title = "Connection";?>

<?php if ($role == "organisateur" || $role == "entraineur"): ?>

<form action="index.php" method="post">
<input type='hidden' name='controller' value='creationArbitre' />
<input type='hidden' name='action' value='enregistrerArbitre' />
    <div>
        <label for="nomArbitre">Nom: </label>
        <input type="text" name="nomArbitre" id="nomArbitre" required>
    </div>
    <div>
        <label for="prenomArbitre">Prénom: </label>
        <input type="text" name="prenomArbitre" id="prenomArbitre" required>
    </div>
    <div>
        <label for="nationaliteArbitre">Nationalité:</label>
        <input type="text" id="nationaliteArbitre" name="nationaliteArbitre" />
    </div>
  <div>
    <input type="submit" value="Creer un arbitre">
  </div>
</form>

<?php else: ?>
    <?php header("Location: index.php?controller=Base&action=login")?>
<?php endif ?>