  
<?php $this->title = "Connection";?>

<?php if ($role == "organisateur"): ?>
<script  src="/asset/JS/logic.js"></script>
<div class="containeur">
  <form onsubmit="return checkFormulaireCreateFDM()" action="index.php" method="post" >
  <input type='hidden' name='controller' value='CreationDeMatch' />
  <input type='hidden' name='action' value='EnregistrerFeuilleDeMatch' />
  <div class="row" id="ligneFormulaire">
    <div class="col-lg-3"><label for="lieu">Lieu de la rencontre: </label></div>
    <div class="col-lg-3"><input type="text" name="lieu" id="lieu" required></div>
  </div>
  <div class="row" id="ligneFormulaire">
  <div class="col-lg-3"><label for="dateRencontre">Date de la rencontre: </label></div>
  <div class="col-lg-3"><input type="date" name="dateRencontre" id="dateRencontre" required></div>
  </div>
  <div class="row" id="ligneFormulaire">
    <div class="col-lg-3"><label for="equipe">Choisis la premiere equipe:</label></div>
    <div class="col-lg-3"><input list="equipe" id="equipe1" name="equipe1" /></div>
    <div class="col-lg-3"><label for="equipe">Choisis la seconde equipe:</label></div>
    <div class="col-lg-3"><input list="equipe" id="equipe2" name="equipe2" /></div>
  </div>
  <div class="row" id="ligneFormulaire">
  <div class="col-lg-3"><label for="arbitre">Choisis l'arbitre principal:</label></div>
  <div class="col-lg-3"><input list="arbitre" id="arbitre0" name="arbitre0" /></div>
  </div>  
  <div class="row" id="ligneFormulaire">
    <div class="col-lg-3"><label for="arbitre">Choisis le premiere arbitre secondaire:</label></div>
    <div class="col-lg-3"><input list="arbitre" id="arbitre1" name="arbitre1" /></div>
    <div class="col-lg-3"><label for="arbitre">Choisis le second arbitre secondaire:</label></div>
    <div class="col-lg-3"><input list="arbitre" id="arbitre2" name="arbitre2" /></div>
  </div>
  </div>
  <datalist id="equipe" id="ligneFormulaire">
  <?php foreach($equipes as $value): ?>
    <option value="<?=$value->getNom()?>">
  <?php endforeach; ?>
  </datalist>
  <datalist id="arbitre">
    <?php foreach($arbitres as $value): ?>
    <option value="<?=$value->getFullName()?>">
    <?php endforeach; ?>
  </datalist>
    <div class="row" id="ligneFormulaire">
      <div class="col-lg-9"></div>
      <div class="col-lg-2"><input type="submit" value="Creer une rencontre"></div>
    </div>
  </form>
</div>
<?php elseif($role == "entraineur"): ?>
      <?php header("Location: index.php?controller=ListeMatch") ?>
    <?php else: ?>
    <?php header("Location: index.php?controller=Base&action=login")?>
<?php endif ?>