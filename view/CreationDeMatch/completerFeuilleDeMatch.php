<?php $this->title = "Completer une feuille de match"?>
<?php if ($role == "organisateur" || $role == "entraineur"): ?>
<script  src="/asset/JS/logic.js"></script>
<div class="col-lg-12">
    <?php 
        if($erreur==true){
            header("Location: index.php?controller=ListeMatch");
        }
    ?>
    <span id="dataUseful" data-joueur='<?=json_encode($joueur)?>' data-equipe='<?=json_encode($equipe)?>'>
<?php if($role == "organisateur"):?>
              <label for="equipe">Choisir l'equipe a modifier:</label>
                <select id="equipe" name="equipe" onchange="changeJoueur()">
            <?php foreach($equipe as $value): ?>
                <option value="<?=$value->getNom()?>"><?=$value->getNom()?></option>
            <?php endforeach; ?>
            </select>
        <?php else:?>
            <label for="equipe"><?=$equipe->getNom()?></label>
            <input type='hidden' name='equipe' value='<?=$equipe->getIdentifiant() ?>'></input>
        <?php endif;?>
            </span>
        </div>
            <div class="col-lg-2" id="boxListeJoueur">
            <div id="joueurASelectionner" >
    <?php if($role == "organisateur"):?>
            <?php foreach($joueur[0] as $value): ?>
                <div id="<?=$value->getIdentifiant()?>" draggable="true" ondragstart="drag(event)"><label id="JoueurDansListe"><?=$value->getNom()?>(<?=$value->getNumero()?>)</label></div>
            <?php endforeach;?>
        <?php else: ?>
            <?php foreach($joueur as $value): ?>
                <div id="<?=$value->getIdentifiant()?>" draggable="true" ondragstart="drag(event)"><label id="JoueurDansListe"><?=$value->getNom()?>(<?=$value->getNumero()?>)</label></div>
            <?php endforeach;?>
        <?php endif;?>
        </div>
            </div>
        <form id="formFMD" onsubmit="return checkFormulaireFDM()" action="index.php" method="post">
        <input type='hidden' name='controller' value='CreationDeMatch' />
        <input type='hidden' name='action' value='enregistrerModification' />
        <input type='hidden' name='id' value='<?=$id?>' />
        <label>Choisir le capitaine :</label>
        <input list='joueurCapitaine' name='capitaine' required/>
        <label>Choisir l'adjoint :</label>
        <input list='joueurCapitaine' name='adjoint' required/>
        <datalist id="joueurCapitaine">
    <?php if($role == "organisateur"):?>
          <?php foreach($joueur[0] as $value): ?>
          <option value="<?=$value->getNom()?> <?=$value->getPrenom()?>">
          <?php endforeach; ?>
        <?php else: ?>
          <?php foreach($joueur as $value): ?>
          <option value="<?=$value->getNom()?> <?=$value->getPrenom()?>">
          <?php endforeach; ?>
        <?php endif;?>
        </datalist>
        <div>
        <BR>
        <div class="col-lg-10" id="terrain">
            <div class="row col-lg-12" id="attaquant">
            <div class="col-lg-1"></div> 
                <div class="col-lg-3" id="boxPoste">
                    <label>Ailier gauche</label>
                    <div id="dropZone_ailier_gauche" style="border-Style:solid;" ondrop="drop(event,'ailier_gauche')" ondragover="allowDrop(event)">
                      
                </div>
                </div>
                <div class="col-lg-1"></div>    
                <div class="col-lg-3" id="boxPoste">
                    <label>Avant centre</label>
                    <div id="dropZone_avant_centre" style="border-Style:solid;" ondrop="drop(event,'avant_centre')" ondragover="allowDrop(event)">

                    </div>
                </div>
                <div class="col-lg-1"></div>  
                <div class="col-lg-3" id="boxPoste">
                    <label>Ailier droit</label>
                    <div id="dropZone_ailier_droit" style="border-Style:solid;" ondrop="drop(event,'ailier_droit')" ondragover="allowDrop(event)">

                    </div>
                </div>
            </div>

            <div id="milieu" class="row col-lg-12">
             
                <div class="row">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-3" id="boxPoste">
                        <label>Milieu Offensif</label>
                        <div id="dropZone_milieu_offensif" style="border-Style:solid;" ondrop="drop(event,'milieu_offensif')" ondragover="allowDrop(event)">
                           
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-1"></div> 
                    <div class="col-lg-3" id="boxPoste">
                        <label>Milieu gauche</label>
                        <div id="dropZone_milieu_gauche" style="border-Style:solid;" ondrop="drop(event,'milieu_gauche')" ondragover="allowDrop(event)">
                            
                        </div>
                    </div>
                    <div class="col-lg-1"></div> 
                    <div class="col-lg-3" id="boxPoste">
                        <label>Milieu centre</label>
                        <div id="dropZone_milieu_centre" style="border-Style:solid;" ondrop="drop(event,'milieu_centre')" ondragover="allowDrop(event)">
                            
                        </div>
                    </div>
                    <div class="col-lg-1"></div> 
                    <div class="col-lg-3" id="boxPoste">
                        <label>Milieu droit</label>
                        <div id="dropZone_milieu_droit" style="border-Style:solid;" ondrop="drop(event,'milieu_droit')" ondragover="allowDrop(event)">
                            
                        </div>
                    </div>
                </div>
                <div class="row" id="boxPoste">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-3" >    
                        <label>Milieu Défensif</label>
                        <div id="dropZone_milieu_defensif" style="border-Style:solid;" ondrop="drop(event,'milieu_defensif')" ondragover="allowDrop(event)">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div id="defense" class="row col-lg-12">
            <div class="col-lg-1"></div> 

                <div class="col-lg-3" id="boxPoste">
                    <label>Arrière gauche</label>
                    <div id="dropZone_arriere_gauche" style="border-Style:solid;" ondrop="drop(event,'arriere_gauche')" ondragover="allowDrop(event)">
                        
                    </div>
                </div>
                <div class="col-lg-1"></div> 
                    <div class="col-lg-3" id="boxPoste">
                    <label>Défenseur Central</label>
                    <div id="dropZone_defenseur_central" style="border-Style:solid;" ondrop="drop(event,'defenseur_central')" ondragover="allowDrop(event)">
                        
                    </div>
                </div>
                <div class="col-lg-1"></div> 
                <div class="col-lg-3" id="boxPoste">
                    <label>Arrière droit</label>
                    <div id="dropZone_arriere_droit" style="border-Style:solid;" ondrop="drop(event,'arriere_droit')" ondragover="allowDrop(event)">
                        
                    </div>
                </div>
            </div>
            <div id="gardien" class="row col-lg-12">
                <div class="col-lg-5"></div>
                <div class="col-lg-3" id="boxPoste">
                    <label>Gardien</label>
                    <div id="dropZone_Gardien" style="border-Style:solid;" ondrop="drop(event,'Gardien')" ondragover="allowDrop(event)">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2"></div>
        <div class="col-lg-3">
            <label>Remplaçants</label>
            <div id="dropZone_remplacant" style="border-Style:solid;" ondrop="drop(event,'remplacant')" ondragover="allowDrop(event)">
                        
            </div>
        </div>
        <div class="col-lg-2"></div>
        <div class="col-lg-2">
            <input id="saveButton" type="submit" value="Enregister la composition">
        </div>
        </form>
    </div>
<?php else: ?>
    <?php header("Location: index.php?controller=Base&action=login")?>
<?php endif ?>