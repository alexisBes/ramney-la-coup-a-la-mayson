<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <base href="<?= $webRoot ?>" >
    <title><?= $title ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/style.css" rel="styleSheet">
</head>
<body>
    <img src="asset/img/background.jpg" id="background"/>
<header id="ban">
    
    <div class="container"> 
        <div class="row">   
            <div class="col-lg-2">
                <img src="asset/img/logo1.png" id="logoEntete"/>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <h1>Fédération Francaise</h1>
                <h1>De</h1>
                <h1>FootBall</h1>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2 visible-lg">
                <img src="asset/img/logo2.png" id="logoEntete"/>
            </div>
        </div>
    </div>
</header>
<nav class="row" id="nav">
        <div class="col-lg-1"></div>
        <div class="col-lg-2" id="navButton">
            <a id="navText" href="index.php?controller=ListeMatch" id="test">Liste des matchs</a>
        </div>

<?php if ($role == "organisateur"): ?>
        <div class="col-lg-2" id="navButton">
            <a id="navText" href="index.php?controller=CreationDeMatch">Créer une rencontre</a>
        </div>
        <?php endif?>
<?php if ($role == "organisateur"||$role == "entraineur"): ?>
        <div class="col-lg-2" id="navButton">
            <a id="navText" href="index.php?controller=Base&action=logout">Déconnexion</a>
        </div>        
<?php else: ?>
        <div class="col-lg-2" id="navButton">
            <a id="navText" href="index.php?controller=Base&action=login">Connexion</a>
        </div>
<?php endif ?>
    </ol>
</nav>

    <div id="contentBackground" class="container">
        <?= $content ?>
    </div>
    <div id="spacebeforeFooter"></div>
    <script src="asset/PDFPrinter/dist/html2pdf.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="asset/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>