<?php $this->title = "Index";?>
<?php    $Match;
        $Event;
        
?>
<script  src="/asset/JS/logic.js"></script>

<?php if($role == "organisateur" || $role == "entraineur"): ?>
 <button onClick="printDocument()">Impression</button>
<?php endif ?>

<div id="toPrint">

<?php if($Match->getDateRencontre()>date('Y-m-d')):?>
    <?php header("Location: index.php?controller=ListeMatch")?> 
<?php endif?>
<div id="encadreMatch" class="container">
    <div class="col-lg-2" id="infosEncadre">
        <label id="ContenuEncadre"><?=$Match->getEquipe1()->getNom()?></label>
        <img src="asset/img/equipe/<?=$Match->getEquipe1()->getNom()?>.png" id="logoEquipe" />
    </div>
    <div class="col-lg-8" id="infosEncadre">
        <div><label id="ContenuEncadre"><?=$Match->getDateRencontre()?></label></div>
        <div><label id="ContenuEncadre"><?=$Match->getLieu()?></label></div>
        <div><label id="ContenuEncadreScore"><?=$Match->getButEquipe1()?>-<?=$Match->getButEquipe2()?></label></div>
    </div>
    <div class="col-lg-2" id="infosEncadre">
        <label id="ContenuEncadre"><?=$Match->getEquipe2()->getNom()?></label>
        <img src="asset/img/equipe/<?=$Match->getEquipe2()->getNom()?>.png" id="logoEquipe" />
    </div>
</div>
<div id="EventArea">
<?php
    foreach($Event as $e):
?>
<div id="encadreEvenement" class="container">
    <label id="TextEvenement"><?=$e->getEvenement()->getEvenType()?> de <?= $e->getJoueur()->getNom()?> <?=$e->getJoueur()->getPrenom()?>,<?=$e->getJoueur()->getPoste()?> , à la <?=$e->getTemps()?>ème minute.</label>
</div>
<?php endforeach;?>
    </div>
    </div>
<?php if($role =="organisateur"): ?>
<div id="data" class="container" data-idMatch='<?=$Match->getIdentifiant()?>' data-equipe1='<?=json_encode($Match->getEquipe1())?>' data-equipe2='<?=json_encode($Match->getEquipe2())?>' 
    data-joueurs1='<?=json_encode($joueurs1)?>' data-joueurs2='<?=json_encode($joueurs2)?>' data-event='<?=json_encode($arrayEvent)?>'>

<div id="formulaireAera">
    
</div>
    <div  class="col-lg-12" id="infosEncadre">
        <button id="ContenuEncadre" onClick="AddFomulaire()">Ajouter un evenement</button>
    </div>
</div>
<?php endif ?>

