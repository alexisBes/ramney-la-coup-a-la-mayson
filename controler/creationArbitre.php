<?php
    spl_autoload_register('loadController');
    spl_autoload_register('loadManager');
    
    class creationArbitre extends controler{
        public function index(){
            session_start();
            $this->generateView(array("role"=>$_SESSION["role"]));
        }

        public function enregistrerArbitre(){
            $nomArbitre = $this->request->getParameter("nomArbitre");
            $prenomArbitre = $this->request->getParameter("prenomArbitre");
            $nationaliteArbitre = $this->request->getParameter("nationaliteArbitre");
            $arbitreManager = new arbitreManager();
            $arbitreManager->insert($nomArbitre, $prenomArbitre, $nationaliteArbitre);
            $this->generateView(array());
        }
    } 
?>