<?php

spl_autoload_register('loadManager');
spl_autoload_register('loadEntity');

class Async extends Controler
{
    public function index(){
        $posteManager = new posteManager();
        $eventManager = new evenementManager();
        $joueurManager = new joueursManager();
        $estArriveManager = new estArriveManager();
        $idJoueur =  $this->request->getParameter("idJoueur");
        $idMatch =  $this->request->getParameter("idMatch");
        $idEvent = $this->request->getParameter("idEvent");
        $time =  $this->request->getParameter("time");
        $post = $posteManager->selectByjoueurIdAndFdmId($idJoueur,$idMatch);
        $joueur = $joueurManager->selectOneByID($idJoueur);
        $joueur->setPoste($posteManager->selectByjoueurIdAndFdmId($joueur->getIdentifiant(),$idMatch)->getIntitule());
        $evenement = $eventManager->selectByEvenementId($idEvent);
        $estArrive = new estarrive(array("idPoste"=>$post->getIdentifiant(),"idEvenement"=>$evenement->getIdentifiant(),"temps"=>$time));
        $estArriveManager->insertOne($estArrive);
        $stringToReturn = '<div id="encadreEvenement" class="container"><label id="TextEvenement">'.$evenement->getEvenType() .' de '. $joueur->getNom().' '.$joueur->getPrenom().', '.$joueur->GetPoste() .', à la '. $estArrive->getTemps().'ème minute</label></div>';
        echo $stringToReturn;
    }
}
?>