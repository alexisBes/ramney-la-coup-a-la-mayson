<?php

    spl_autoload_register('loadController');
    spl_autoload_register('loadManager');

    class DetailMatchPasse extends Controler{
        public function index(){
            session_start();
            if(isset($_SESSION["role"]))
                $role = $_SESSION["role"];
            else
                $role = "";
            $idMatch = $this->request->getParameter("id");
            $FeuilleDeMatchManager = new feuilleDeMatchManager();
            $equipeManager = new equipeManager();
            $estArriveManager = new estArriveManager();
            $JoueurManager = new joueursManager();
            $EvenementManager = new evenementManager();
            $posteManager = new posteManager();

            //récupération des informations du match
            $Match = $FeuilleDeMatchManager->selectFDMById($idMatch);
            $TabEquipe = $equipeManager->selectByFDMId($Match->getIdentifiant());
            $Match->setEquipe1($TabEquipe[0]);
            $Match->setEquipe2($TabEquipe[1]);
            $joueurs1 = $JoueurManager->selectFirstTeamPlayer($TabEquipe[0]->getIdentifiant(),$Match->GetIdentifiant());
            $joueurs2 = $JoueurManager->selectFirstTeamPlayer($TabEquipe[1]->getIdentifiant(),$Match->GetIdentifiant());

            if (!(empty($joueurs1))) {
                foreach ($joueurs1 as $value) {
                    $poste = $posteManager->selectByjoueurIdAndFdmId($value->getIdentifiant(),$Match->getIdentifiant());
                    $value->setPoste($poste->getIntitule());
                }
            }
            if (!(empty($joueurs2))) {
                foreach ($joueurs2 as $value) {
                    $poste = $posteManager->selectByjoueurIdAndFdmId($value->getIdentifiant(),$Match->getIdentifiant());
                    $value->setPoste($poste->getIntitule());
                }
            }
            //récupération des evenements du match
            $arrayEvent = $EvenementManager->selectAll();
            $TEvenements = $estArriveManager->selectByFDMId($idMatch);
            foreach($TEvenements as $Evenement){
                $idposte = $Evenement->getIdPoste();
                $joueur = $JoueurManager->selectByPosteId($idposte);
                $Evenement->setJoueur($joueur);
                $Evenement->getJoueur()->setPoste($posteManager->selectByjoueurIdAndFdmId($joueur->getIdentifiant(),$Match->getIdentifiant())->getIntitule());
                $Evenement->setEvenement($EvenementManager->selectByEvenementId($Evenement->getIdEvenement()));
            }

            //récupération des buts
            $Match->setButEquipe1($estArriveManager->getbutByIdMatchAndIdEquipe($idMatch,$TabEquipe[0]->getIdentifiant()));
            $Match->setButEquipe2($estArriveManager->getbutByIdMatchAndIdEquipe($idMatch,$TabEquipe[1]->getIdentifiant()));
            $this->generateView(array("role"=>$role,"Match"=>$Match,"joueurs1"=>$joueurs1,"joueurs2"=>$joueurs2,"Event"=>$TEvenements,"arrayEvent"=>$arrayEvent));
        }
    }

?>