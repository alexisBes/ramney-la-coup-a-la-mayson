<?php
    spl_autoload_register('loadController');
    spl_autoload_register('loadManager');


class CreationDeMatch extends Controler
{
    public function index()
    {
        $manEquipe = new equipeManager();
        $equipes = $manEquipe->selectAll();
        $manArbitre = new arbitreManager();
        $arbitres = $manArbitre->SelectAll();
        session_start();
        if(isset($_SESSION["role"]))
            $role = $_SESSION["role"];
            else
            $role ="";
        $this->generateView(array("role"=>$role,"equipes"=>$equipes,"arbitres"=>$arbitres));
    }

    public function EnregistrerFeuilleDeMatch()
    {
        $Fdm =$this->request->getObject("feuilleDeMatch");
        $equipManager = new equipeManager();
        $joueurManager = new joueursManager();
        $fdmManager = new feuilleDeMatchManager();
        $arbitreManager = new arbitreManager();
        $arbitrerManager = new arbitrerManager();
        $posteManager = new posteManager();

        $arrayJoueursEquipe1 = $joueurManager->selectByEquipeId($equipManager->selectByName($Fdm->getEquipe1())->getIdentifiant());
        $arrayJoueursEquipe2 = $joueurManager->selectByEquipeId($equipManager->selectByName($Fdm->getEquipe2())->getIdentifiant());
        $arrayArbitres = array();
        $fdmManager->InsertOne($Fdm);
        for ($i=0; $i < 3; $i++) {
            $name = explode(' ', $this->request->getParameter('arbitre'.$i));
            $isPrimary = ($i==0)?1:0;
            $data = array("estPrincipal"=>$isPrimary,"idFDM"=>$Fdm->getIdentifiant(),"idArbitre"=>$arbitreManager->SelectByNAme($name[0],$name[1])[0]->getIdentifiant());
            $arbitrer = new arbitrer($data);
            array_push($arrayArbitres,$arbitrer );
        }
        $arbitrerManager->InsertAll($arrayArbitres);
        $arrayPoste = array();
        foreach ($arrayJoueursEquipe1 as $value) {
            $data = array("intitule"=>"banc","estRemplacant"=>1,"idJoueurs"=>$value->getIdentifiant(),"idFDM"=>$Fdm->getIdentifiant());
            $poste = new poste($data);
            array_push($arrayPoste,$poste);
        }
        foreach ($arrayJoueursEquipe2 as $value) {
            $data = array("intitule"=>"banc","estRemplacant"=>1,"idJoueurs"=>$value->getIdentifiant(),"idFDM"=>$Fdm->getIdentifiant());
            $poste = new poste($data);
            array_push($arrayPoste,$poste);
        }
        $posteManager->InsertAll($arrayPoste);
        $this->generateView(array());
    }

    public function completerFeuilleDeMatch()
    {
        $id = $this->request->getParameter("id");
        session_start();
        if(isset($_SESSION["role"]))
            $role = $_SESSION["role"];
        else
            $role ="";
        $equipManager = new equipeManager();
        $joueurManager = new joueursManager();
        $equipe = $equipManager->selectByFDMId($id);
        $erreur=true;
        if(isset($_SESSION["equipe"])){
            if($equipe[0]->getIdentifiant()==$_SESSION["equipe"] || $equipe[1]->getIdentifiant()==$_SESSION["equipe"]){
                $erreur=false;
            }
        }
        if ($role == "entraineur") {
            $equipe = $equipManager->selectById($_SESSION["equipe"]);
            $joueur = $joueurManager->selectByEquipeId($equipe->getIdentifiant());
        }
        if ($role == "organisateur") {
            $id = $this->request->getParameter("id");
            $erreur=false;
            $arrayJoueur1 =  $joueurManager->selectByEquipeId($equipe[0]->getIdentifiant());
            $arrayJoueur2 =  $joueurManager->selectByEquipeId($equipe[1]->getIdentifiant());
            $joueur = array($arrayJoueur1,$arrayJoueur2);
        }
        if($role == "")
        {
            $equipe = 0;
            $joueur=0;
        }
        $this->generateView(array("role"=>$role,"id"=>$id,"equipe"=>$equipe,"joueur"=>$joueur, "erreur"=>$erreur));
    }

    public function enregistrerModification()
    {
        $array = $this->request->getParameter("joueurs");
        $id = $this->request->getParameter("id");
        $nameCapitaine = $this->request->getParameter("capitaine");
        $nameAdjoint = $this->request->getParameter("adjoint");
        $posteManager = new posteManager();
        $joueurManager = new joueursManager();
        foreach ($array as $role => $value) {
            foreach ($value as $idJoueur) {
                $poste = $posteManager->selectByjoueurIdAndFdmId($idJoueur,$id);
                $poste->setIntitule($role);
                $poste->setEstRemplacant($role=="banc"?1:0);
                $posteManager->Update($poste);
            }
        }
        $partCapitaine = explode(' ',$nameCapitaine);
        $capitaine = $joueurManager->selectOneByNameAndIDEquipe($partCapitaine[0],$partCapitaine[1]);
        $partAdjoint= explode(' ',$nameAdjoint);
        $adjoint =$joueurManager->selectOneByNameAndIDEquipe($partAdjoint[0],$partAdjoint[1]);
        $capitaine->setRole("capitaine");
        $joueurManager->update($capitaine);
        $adjoint->setRole("adjoint");
        $joueurManager->update($adjoint);
        $this->generateView(array());
    }
}


?>