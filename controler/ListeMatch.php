<?php // si tu le mets pas, il comprend pas le pauvre
    spl_autoload_register('loadController');
    spl_autoload_register('loadManager');

    
    class ListeMatch extends Controler{
        public function index(){
            session_start();
            if(isset($_SESSION["role"]))
                $role = $_SESSION["role"];
            else
                $role = "";
            $TabFDM = array();
            $FDMManager = new feuilleDeMatchManager();
            $equipeManager = new equipeManager();
            $estArriveManager = new estArriveManager();
            $TabFDM = $FDMManager->selectAll();
            foreach($TabFDM as $FDM){
                
                $TabEquipe = $equipeManager->selectByFDMId($FDM->getIdentifiant());
                
                $FDM->setEquipe1($TabEquipe[0]);
                $FDM->setEquipe2($TabEquipe[1]);
                $FDM->setButEquipe1($estArriveManager->getbutByIdMatchAndIdEquipe($FDM->getIdentifiant(),$TabEquipe[0]->getIdentifiant()));
                $FDM->setButEquipe2($estArriveManager->getbutByIdMatchAndIdEquipe($FDM->getIdentifiant(),$TabEquipe[1]->getIdentifiant()));
            }
           
            $this->generateView(array("Data"=>$TabFDM,"role"=>$role));
        }
    }
?>