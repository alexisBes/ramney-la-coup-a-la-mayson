<?php
spl_autoload_register('loadController');
spl_autoload_register('loadManager');


class Base extends Controler
{
    public function index(){
        $role ="";
        if(isset($_SESSION["role"]))
            $role = $_SESSION["role"];
         
            $this->generateView(array());    
    }

    public function login()
    {        
        $this->generateView(array());
    }

    public function logout(){
        session_start();
        session_unset();   
        $this->generateView(array());

    }

    public function VerifyAccount()
    {
        $name="";
        $password = $this->request->getParameter("password");
        $role = $this->request->getParameter("role");
        $bconnect = false;
        $erreur=true;
        if ($role == "organisateur") {
            $name = $this->request->getParameter("name");
            $orga = new OrganisateurManageur();
            $orgaFound = $orga->Select($name);
            if($orgaFound!=null){
                $bconnect= $orgaFound->IsValidUser($password);
            }
        }
        if ($role == "entraineur") 
        {
            $name = $this->request->getParameter("name");
            $surname = $this->request->getParameter("surname");
            $entraineurManger = new entraineurManager();
            $entraineurFound = $entraineurManger->SelectByName($surname,$name);
            if($entraineurFound!=null){
                $bconnect= $entraineurFound->IsValidUser($password);
            }
        }
        if ($bconnect) {
            if(session_status() === PHP_SESSION_NONE)
            session_start();
            $erreur=false;
            if($role =="entraineur")
            {
                $_SESSION["equipe"] = $entraineurFound->getIdEquipe();
            }
            $_SESSION["role"] = $role;
        }
        $this->generateView(array("nom"=>$name,"erreur"=>$erreur));
    }
}


?>