<?php
require_once 'entity/entity.php';


    class organisateur extends entity{
        private $identifiant;
        private $mdp;

        public function getIdentifiant(){return $this->identifiant;}
        public function setIdentifiant($value){$this->identifiant=$value;}

        public function getMdp(){return $this->mdp;}
        public function setMdp($value){$this->mdp=$value;}
        public function isValidUser($password)
        {
                $isLog = password_verify($password,$this->mdp);
                return $isLog;
        }
    }
?>