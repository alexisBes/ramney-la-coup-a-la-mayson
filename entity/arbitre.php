<?php
require_once 'entity/entity.php';

    class arbitre extends entity{
        private $identifiant;
        private $nom;
        private $prenom;
        private $nationalite;

        public function getIdentifiant(){return $this->identifiant;}
        public function setIdentifiant($value){$this->identifiant=$value;}

        public function getNom(){return $nom;}
        public function setNom($value){$this->nom=$value;}

        public function getPrenom(){return $this->prenom;}
        public function setPrenom($value){$this->prenom=$value;}

        public function getNationalite(){return $this->nationalite;}
        public function setNationalite($value){$this->nationalite=$value;}

        public function getFullName(){return ($this->prenom." ".$this->nom);}
    }
?>