<?php
require_once 'entity/entity.php';


    class estarrive extends entity{
        //variable de BDD
        private $identifiant;
        private $idPoste;       
        private $idEvenement;
        private $temps;
        //variables utilisées pour affichage
        private $joueur;
        private $evenement;

        public function getIdentifiant(){return $this->identifiant;}
        public function setIdentifiant($value){$this->identifiant=$value;}

        public function getIdPoste(){return $this->idPoste;}
        public function setIdPoste($value){$this->idPoste=$value;}

        public function getIdEvenement(){return $this->idEvenement;}
        public function setIdEvenement($value){$this->idEvenement=$value;}

        public function getTemps(){return $this->temps;}
        public function setTemps($value){$this->temps=$value;}

        public function getJoueur(){return $this->joueur;}
        public function setJoueur($value){$this->joueur=$value;}

        public function getEvenement(){return $this->evenement;}
        public function setEvenement($value){$this->evenement=$value;}
    }
?>