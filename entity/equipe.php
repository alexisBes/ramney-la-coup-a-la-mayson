<?php
require_once 'entity/entity.php';


    class equipe extends entity implements JsonSerializable{
        private $identifiant;
        private $nom;
        private $ville;
        private $pays;

        public function getIdentifiant(){return $this->identifiant;}
        public function setIdentifiant($value){$this->identifiant=$value;}

        public function getNom(){return $this->nom;}
        public function setNom($value){$this->nom=$value;}

        public function getVille(){return $this->ville;}
        public function setVille($value)
        {
            $this->ville=str_replace(' ','_',$value);
        }

        public function getPays(){return $this->pays;}
        public function setPays($value){$this->pays=$value;}

        public function jsonSerialize()
        {
            return 
            [
                'id'   => $this->getIdentifiant(),
                'nom' => $this->getNom(),
                'ville' =>$this->getVille(),
                'pays' =>$this->getPays()
            ];
        }
    }
?>