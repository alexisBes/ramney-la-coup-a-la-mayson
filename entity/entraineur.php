<?php
require_once 'entity/entity.php';


    class entraineur extends entity
    {
        private $identifiant;
        private $nom;
        private $prenom;
        private $estAdjoint;
        private $idEquipe;
        private $mdp;

        public function getIdentifiant(){return $this->identifiant;}
        public function setIdentifiant($value){$this->identifiant=$value;}

        public function getNom(){return $this->nom;}
        public function setNom($value){$this->nom=$value;}

        public function getPrenom(){return $this->prenom;}
        public function setPrenom($value){$this->prenom=$value;}

        public function getEstAdjoint(){return $this->estAdjoint;}
        public function setEstAdjoint($value){$this->estAdjoint=$value;}

        public function getIdEquipe(){return $this->idEquipe;}
        public function setIdEquipe($value){$this->idEquipe=$value;}

        public function getMdp(){return $this->mdp;}
        public function setMdp($value){$this->mdp=$value;}

        public function isValidUser($password)
        {
            $isLog = password_verify($password,$this->mdp);
            return $isLog;
        }
    }
?>