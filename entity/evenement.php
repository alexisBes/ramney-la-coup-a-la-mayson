<?php
require_once 'entity/entity.php';


    class evenement extends entity implements JsonSerializable{
        private $identifiant;
        private $evenType;

        public function getIdentifiant(){return $this->identifiant;}
        public function setIdentifiant($value){$this->identifiant=$value;}

        public function getEvenType(){return $this->evenType ;}
        public function setEvenType($value){$this->evenType=$value;}

        public function jsonSerialize()
        {
            return 
            [
                'id'   => $this->getIdentifiant(),
                'evenType' => $this->getEvenType()
            ];
        }
    }
?>