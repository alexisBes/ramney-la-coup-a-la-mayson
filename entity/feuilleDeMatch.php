<?php
require_once 'entity/entity.php';


    class feuilleDeMatch extends entity
    {
        private $identifiant;
        private $lieu;
        private $dateRencontre;
        private $equipe1;
        private $equipe2;
        private $butEquipe1;
        private $butEquipe2;

        public function getIdentifiant(){return $this->identifiant;}
        public function setIdentifiant($value){$this->identifiant=$value;}

        public function getLieu(){return $this->lieu;}
        public function setLieu($value){$this->lieu=$value;}

        public function getDateRencontre(){return $this->dateRencontre;}
        public function setDateRencontre($value){$this->dateRencontre=$value;}

        public function getEquipe1(){return $this->equipe1;}
        public function setEquipe1($value){$this->equipe1=$value;}

        public function getEquipe2(){return $this->equipe2;}
        public function setEquipe2($value){$this->equipe2=$value;}

        public function getButEquipe1(){return $this->butEquipe1;}
        public function setButEquipe1($value){$this->butEquipe1=$value;}

        public function getButEquipe2(){return $this->butEquipe2;}
        public function setButEquipe2($value){$this->butEquipe2=$value;}
    }
?>