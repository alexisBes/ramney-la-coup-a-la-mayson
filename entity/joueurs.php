<?php
require_once 'entity/entity.php';


    class joueurs extends entity implements JsonSerializable{
        private $identifiant;
        private $nom;
        private $prenom;
        private $ville;
        private $idEquipe;
        private $numero;
        private $role;
        // pas dans la BDD
        private $poste;

        public function getIdentifiant(){return $this->identifiant;}
        public function setIdentifiant($value){$this->identifiant=$value;}

        public function getNom(){return $this->nom;}
        public function setNom($value){$this->nom=$value;}

        public function getPrenom(){return $this->prenom;}
        public function setPrenom($value){$this->prenom=$value;}

        public function getVille(){return $this->ville;}
        public function setVille($value){$this->ville=$value;}

        public function getIdEquipe(){return $this->idEquipe;}
        public function setIdEquipe($value){$this->idEquipe=$value;}

        public function getNumero(){return $this->numero;}
        public function setNumero($value){$this->numero=$value;}
        
        public function getRole(){return $this->role;}
        public function setRole($value){$this->role=$value;}
        
        public function getPoste(){return $this->poste;}
        public function setPoste($value){$this->poste=$value;}

        public function jsonSerialize()
        {
            return 
            [
                'id'   => $this->getIdentifiant(),
                'nom' => $this->getNom(),
                'prenom' => $this->getPrenom(),
                'ville' => $this->getVille(),
                'idEquipe' =>$this->getIdEquipe(),
                'numero' =>$this->getNumero(),
                'role'=>$this->getRole(),
                'poste'=>$this->getPoste()
            ];
        }
    }
?>