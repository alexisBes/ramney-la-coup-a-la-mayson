<?php
require_once 'entity/entity.php';


    class poste extends entity{
        private $identifiant;
        private $intitule;
        private $estRemplacant;
        private $idJoueurs;
        private $idFDM;

        public function getIdentifiant(){return $this->identifiant;}
        public function setIdentifiant($value){$this->identifiant=$value;}

        public function getIntitule(){return $this->intitule;}
        public function setIntitule($value){$this->intitule=$value;}

        public function getEstRemplacant(){return $this->estRemplacant;}
        public function setEstRemplacant($value){$this->estRemplacant=$value;}

        public function getIdJoueurs(){return $this->idJoueurs;}
        public function setIdJoueurs($value){$this->idJoueurs=$value;}

        public function getIdFDM(){return $this->idFDM;}
        public function setIdFDM($value){$this->idFDM=$value;}
    }
?>